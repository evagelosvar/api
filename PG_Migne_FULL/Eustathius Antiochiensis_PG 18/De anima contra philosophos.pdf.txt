De anima contra philosophos (ap. Joannem Damascenum, Sacra
parallela) (fragmenta)
ΠΕΡΙ ΨΥΧΗΣ ΚΑΤΑ ΦΙΛΟΣΟΦΩΝ
1 Ὁ Θεὸς τὰ πάντα πληροῖ ὑπ' οὐδενὸς περιοριζόμενος· ὁ γὰρ ὑπὸ τόπου μὴ
περιοριζόμενος πάντως ἐν παντὶ τόπῳ κατὰ τὴν τοῦ (∆αυῒδ) ὑμνολογίαν ἔσται· εἰ
γὰρ ἔξω λέγοιτο τόπου τινός, ἀνάγκη περιορίζεσθαι αὐτὸν ὑπ' ἐκείνου οὗπερ
στερίσκεται· ὅλος οὖν ἐν πᾶσι γινόμενος, οὐδαμῶς ἐξ ἑτέρων εἰς ἑτέρους μεθίσταται
τόπους τὰ πάντα πληρῶν.
2 Οὐ νόμων καὶ χρόνων ῥηταῖς τὸ θεῖον ὑπόκειται περιγραφαῖς.
3 Ἀλλ' ἐπειδὴ σαφῶς ἴσασιν ἁλισκομένους ἐντεῦθεν ἑαυτοὺς,
μεταβαλλόμενοι πλάττουσιν τὸ τῆς λήθης ὕδωρ, ὅ φασιν πίνοντας ἑκάστους τὰς τῶν
οἰκείων βίων γεννήσεις ἀμνημονεῖν. Ὅτι δὲ ταῦτα μυθώδη καὶ πόρρω φιλοσοφίας
ἐστὶν, οὐδεὶς τῶν εὖ φρονούντων ἔοικεν ἀγνοεῖν. Τί γάρ φαμεν, ὦ Πυθαγόραι καὶ
Πλάτωνες· αὐτοὶ τὸ ληθοποιὸν ὕδωρ πεπώκατε λαβόντες; ἢ τοὐναντίον ἐφύγετε
λαθόντες; Εἰ μὲν οὖν ὑμεῖς οἷοί· τε ἐγένεσθε τὸ τῆς πλάνης διαδρᾶναι πόμα,
πρόδηλοι πολλοί τινες· εἰ δὲ τὸν κοινὸν πᾶσιν ἀέρα σπασάμενοι, τὸ τῆς λήθης ὕδωρ
ἐπίετε χανδόν, πῶς καὶ πόθεν μυθοποιεῖτε λαβόντες περὶ ψυχῶν; πόθεν ὅλως ἴστε τὸ
τῶν ῥείθρων τούτων γένος; εἴτε γὰρ μὴ ἐπειράθητε τῆς χρείας αὐτῶν, ἀγνοεῖτε τὴν
τῶν ὑδάτων ἀνάδοσιν· εἴτε τῆς πείρας αὐτῶν ἔσω γεγονότες, λήθῃ τὴν μνήμην τῆς
χρήσεως αὐτῶν ἠφανίσατε καὶ τὴν γνῶσιν ἐξεπτύσατε. Ἀλλ' εἰς ταῦτα βαρβαρώδη
δεισιδαιμονίᾳ τραπέντες ἐσφάλησαν, Αἰγυπτίοις χρησάμενοι καθηγεμόσιν.
4 Ὅτι μὲν οὔκ εἰσιν ἀγέννητοι αἱ ψυχαὶ, τοῦτο δείκνυσιν σαφέστερον ἡ τῶν
ἀρτιγόνων ἡλικία βρεφῶν. Εἰ μὲν γὰρ ἅμα τῷ τεχθῆναι τὰ παιδία, τελείας ἐξῆπτο
φρονήσεως, ἐξῆν ἴσως ὑποκρίνεσθαί τινας ἄνοιαν· εἰ δὲ λείπεται νοῦ καὶ λόγου καὶ
συνέσεως, οὐκ ἄρα καθέστηκαν ἀγέννητοι τὴν φύσιν αἱ ψυχαί. Σύμπαντες οὖν ἴσμεν,
ὅτι τοῖς τῶν σωμάτων ὄγκοις ἡ ψυχὴ συνεκτέταται, διαρκῶς οὐ μεγέθει μόνον ἢ
βραχύτητι μελῶν, ἀλλὰ καὶ ταῖς ἄλλαις τῆς γνώμης ἀλογίαις· ἐς ὅσον μὲν
νεόπλαστα τυγχάνει ταῦτα τὰ βρέφη, πάσης μὲν πανταχῶς ἐννοίας ἀπορεῖ καὶ
βουλῆς· ἡρέμα δὲ καὶ κατ' ὀλίγον προϊόντα νοῦν τε καὶ φρόνησιν προσλαμβάνει καὶ
λόγον. Εἰ δὲ ταῦτα ἴδια μέν ἐστιν ψυχῆς, ἐκ περιουσίας δὲ τὰς κρείττους ἐπιδέχονται
προσθήκας, ἆρ' οὖν ὅτι συναυξάνει μὲν τῷ σώματι, καθάπερ δὲ τοῦτο λεληθότως
ὑπαναλέγεται τὰς δυνάμεις, οὕτω καὶ ἡ ψυχὴ προκόπτουσα τὸν νοῦν· ὅσοι δὲ
μηδέποτε βαίνουσιν ἐπὶ τὸ ἄμεινον, ἀλλὰ μωροὶ διαμένουσιν, δῆλον ὅτι
λελωβημένου τινὸς μορίου ἀτελεῖς ἔχουσι τὰς ψυχὰς, ὥσπερ οἱ τὴν φρενίτιδα
ἀρρωστήσαντες νόσον. Ἀμέλει καὶ τὸ τοῦ πρωτοπλάστου σῶμα σχηματισθὲν ἔκειτο
νεκρὸν, ἄπνουν, ἀκίνητον μέλεσιν μὲν καὶ ἁρμονίᾳ ῥερυθμισμένον ἄκρως, μορφῇ τε
καὶ κάλλει κεκοσμημένον ἐμπρεπῶς ἅτε δὴ καὶ παρ' αὐτοῦ πλάστου γεννηθὲν τοῦ
Θεοῦ πρωτότυπον ἄγαλμα καὶ τῆς θεσπεσιωτάτης εἰκόνος ἐκτυπωθὲν ἀφομοίωμα
κράτιστον, ἀναίσθητον δὲ καὶ ἄφωνον καὶ ἄπνουν· ὡς δὲ εἰς τὸ πρόσωπον αὐτοῦ
δημιουργικῶς ἐνεφύσησεν ὁ Θεὸς, αὐτίκα τὴν κίνησιν εἴληφεν· ἐξ ἐκείνου δὲ βαδίζει
καὶ ἀναπνεῖ καὶ φθέγγεται, ἄρχει λογίζεται πράττει διοικεῖ. 6 Κοινὸν σώματος καὶ
ψυχῆς πάθος ὕπνος.

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

