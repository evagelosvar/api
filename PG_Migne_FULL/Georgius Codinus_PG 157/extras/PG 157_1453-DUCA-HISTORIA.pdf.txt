MICHAELIS DUCAE HISTORIA TURCOBYZANTINA

35 Qšrouj oân parelqÒntoj kaˆ tÁj metopwrinÁj éraj ¢rcoÚshj,
o‡koi di£gwn oÙk ™d…dou ¢n£pausin to‹j blef£roij, ¢ll¦ kaˆ ™n nuktˆ kaˆ ¹mšrv t¾n front…da tÁj PÒlewj e•ce, pîj aÙt¾n l£boi, pîj kÚrioj aÙtÁj gšnoito.
”Eti Ôntoj oân aÙtoà ™n tù policn…J kaˆ o„kodomoàntoj, ™xÁlqen ™k tÁj PÒlewj eŒj tecn…thj Ð t¦j petrobolima…ouj cènaj kataskeu£zwn, tÕ gšnoj Oâggroj, tecn…thj dokimètatoj. Oátoj prÕ polloà ™n tÍ KwnstantinoupÒlei ™lqën
kaˆ shm£naj to‹j mes£zousi tù basile‹ t¾n tšcnhn aÙtoà, ¢nšferon tù basile‹. `O d• basileÝj gr£yaj aÙtù sithršsion oÙk ¥xion prÕj t¾n ™pist»mhn
aÙtoà, oÙd' ™ke‹no tÕ mhdaminÕn kaˆ eÙar…qmhton ™d…dosan tù tecn…tV. “Oqen
kaˆ ¢pognoÚj, katalipën t¾n PÒlin mi´ tîn ¹merîn tršcei prÕj tÕn b£rbaron. Kaˆ aÙtÕj ¢spas…wj ¢podex£menoj kaˆ trof¦j kaˆ ™ndÚmata filotim»saj
aÙtÒn, d…dwsi kaˆ ·Ògan tÒshn, Óshn e„ Ð basileÝj tÕ tštarton œdiden, oÙk ¨n
¢ped…draske tÁj KwnstantinoupÒlewj. 'Erwthqeˆj oân par¦ toà ¹gemÒnoj, e„
dÚnatai kenîsai cwne…an meg£lhn, pštran fšrousan Øpermegšqh, Óson prÕj t¾n
¢lk¾n kaˆ tÕ p£coj toà te…couj tÁj PÒlewj, aÙtÕj d' ¢ntapekr…nato· DÚnamai,
e„ boÚlei, kataskeu©sai cwne…an, Ósh tÕ mšgeqoj tugc£nei tÁj deiknuomšnhj moi
pštraj. 'Egë g¦r t¦ te…ch tÁj PÒlewj ¢kribîj ™p…stamai. OÙ mÒnon g¦r ™ke‹na, ¢ll¦ kaˆ t¦ Babulènia te…ch æj coàn leptune‹ ¹ par¦ tÁj cwne…aj tÁj ™mÁj
¢feqe‹sa. Pl¾n ™gë tÕ p©n toà œrgou kalîj ¢part…sw, t¾n d• bol¾n oÙk ™p…stamai
oÙd• sunt£ssomai.”–Toàto ¢koÚsaj Ð ¹gemën œfh· KataskeÚasÒn moi t¾n cwne…an, perˆ d• tÁj bolÁj toà l…qou aÙtÕj Ôyomai.”–”Hrxanto sunaqro…zein calkÕn to…nun kaˆ Ð tecn…thj tÕn tÚpon tÁj skeuÁj œplatten. 'En trisˆn oân mhsˆ
kateskeu£sqh kaˆ ™cwneÚqh tšraj ti foberÕn kaˆ ™xa…sion.
37 ParelqÒntoj oân toà 'Ianouar…ou mhnÕj kaˆ toà Febrouar…ou
¥rxantoj ™kšleuse t¾n cwne…an metakomisqÁnai ™n tÍ KwnstantinoupÒlei· kaˆ
zeÚxaj ¡m£xaj tri£konta eŒlkon aÙt¾n Ôpisqen oƒ x bÒej, lšgw bÒej boîn· kaˆ
™k plag…ou tÁj cwne…aj ¥ndrej s, kaˆ e„j tÕ n kaˆ e„j tÕ ›teron, toà ›lkein
kaˆ ™xisoàn aÙt»n, ‡na m¾ Ñlisq»sV toà drÒmou· kaˆ œmprosqen tîn ¡maxîn tšktonej n toà kataskeu£zein gefÚraj xul…nouj e„j t¦j ¢nwmal…aj tÁj Ðdoà kaˆ
™rg£tai sÝn a„to‹j s 'Epo…hse goàn tÕn Febrou£rion kaˆ M£rtion, ›wj oá
kat»nthsen ™n tÒpJ makr¦n tÁj pÒlewj ¢pÕ mil…wn e.
38 Taàta m•n di¦ qal£sshj, di¦ d• xhr©j t¾n cwne…an ™ke…nhn t¾n pammegšqh fšrwn, ¢ntikrÝ toà te…couj œsthsen ™n tÍ pÚlV toà ¡g…ou `Rwmanoà
plhs…on. Kaˆ labën shme‹on Ð tecn…thj, e•ce g¦r ™k plag…ou fwle¦j dÚo kateskeuasmšnaj, cwroÚsaj pštraj æj litrîn ... aÙtofuîj tecnasmšnaj, kaˆ Óte
ºboÚleto ¢polÚein t¾n meg£lhn, ™shmeioàto tÕn tÒpon prîton, pšmpwn t¾n mikr£n, kaˆ tÒte stocastikîj ™sfendÒnei t¾n meg…sthn. Kaˆ kroÚsaj t¾n prèthn
bol¾n kaˆ ¢koÚsantej toà ktÚpou oƒ tÁj pÒlewj, ™nneoˆ gegÒnasi kaˆ tÕ KÚrie
™lšhson” œkrazon.
38. 11-12 `O d• sofist¾j tÁj kak…aj ™ke…nhj, Ð tecn…thj t… mšqodon ™p…stato
toà m¾ diarragÁnai t¾n cwne…an; Kaˆ g¦r e‡damen cwne…aj ¢poluoÚsaj probÒlouj· met¦ goàn tÕ ¢poluqÁnai toà skeÚouj, e„ kaˆ m¾ ™ful£tteto skepazomšnh
ØpÕ p…lwn ™x ™r…wn pacšwn, pareuqÝj æj Ûeloj die¸·»gnuto· kaˆ met¦ tosaÚthn
™piskop»n, À dˆj À tÕ plšon trˆj ¢popšmpwn, ™sc…zeto toà ¢šroj cwroàntoj ™n
to‹j ko…loij tîn met£llwn ØpobaqÚmasin. Oátoj d• t… ™po…ei; Met¦ tÕ sfendonisqÁnai t¾n pštran, tÁj cwne…aj zeoÚshj ¢pÕ tÁj qermÒthtoj toà n…trou kaˆ
toà te£fou, pareuqÝ katšbrecen aÙt¾n ™la…J kaˆ sÝn toÚtJ ™plhroànto t¦
œndon aÙtÁj ¢erèdh p£qh kaˆ oÙk ™n»rgei tÕ yucrÕn leanq•n ØpÕ tÁj toà ™la…ou

qermÒthtoj kaˆ ™b£staze tÕn kÒpon eÙkÒlwj ¥crij oá ØpoÚrghse tÕn Ôleqron tÁj
PÒlewj· kaˆ œti met¦ taàta ful£ttetai sèa kaˆ ™nerge‹ prÕj tÕ toà tur£nnou qšlhma.
KroÚsaj oân kaˆ diase…saj tÕ te‹coj, ºboul»qh ™k deutšrou ™n aÙtù tù tÒpJ bale‹n ¥llon ›na l…qon. Tucën d• Ð ¢pokrisi£rioj toà 'I£gkou ™ke‹,
œskwye t¾n bol¾n lšgwn· E„ boÚlei katapese‹n eÙkÒlwj t¦ te…ch, met£qej
t¾n skeu¾n ™n ¥llJ mšrei toà to…cou ¢pšconti ¢pÕ tÁj prèthj bolÁj Ñrgui¦j e
À ›x, kaˆ tÒte ™xisîn t¾n prèthn, ¥fej ˜tšran bol»n. Tîn dÚo oân ¥krwn
krousqšntwn ™pimelîj, tÒte b£lle kaˆ tr…thn, æj eØreqÁnai t¦j tre‹j bol¦j
e„j trigènou scÁma· kaˆ tÒte Ôyei tÕn toioàton to‹con e„j gÁn katap…ptonta.”–”Hresen oân ¹ boul¾ kaˆ oÛtwj pšpracen Ð tecn…thj kaˆ oÛtwj ¢pšbh.

TÕ d• prosfeÚgein ™n tÍ Meg£lV 'Ekklhs…v toÝj p£ntaj, t…; ’Hsan
prÕ pollîn crÒnwn ¢koÚontej par£ tinwn yeudom£ntewn, pîj mšllei ToÚrkoij
paradoqÁnai ¹ pÒlij kaˆ e„selqe‹n ™ntÕj met¦ dun£mewj kaˆ katakÒptesqai toÝj
`Rwma…ouj par' aÙtîn ¥cri toà K…onoj toà Meg£lou Kwnstant…nou. Met¦ d•
taàta katab¦j ¥ggeloj fšrwn ·omfa…an paradèsei t¾n basile…an sÝn tÍ ·omfa…v ¢nwnÚmJ tinˆ ¢ndrˆ eØreqšnti tÒte ™n tù k…oni ƒstamšnJ, l…an ¢per…ttJ
kaˆ penicrù kaˆ ™re‹ aÙtù· Lab• t¾n ·omfa…an taÚthn kaˆ ™kd…khson tÕn laÕn
Kur…ou.”–TÒte trop¾n ›xontai oƒ Toàrkoi kaˆ oƒ `Rwma‹oi katadièxousin aÙtoÝj kÒptontej kaˆ ™xel£sousin kaˆ ™k tÁj PÒlewj kaˆ ¢pÕ tÁj dÚsewj kaˆ ¢pÕ
tîn tÁj ¢natolÁj merîn ¥cri Ðr…wn Pers…aj ™n tÒpJ kaloumšnJ Monodendr…J.
Taàt£ tinej æj ¢pobhsÒmena œcontej, œtrecon kaˆ toÝj ¥llouj ™sumboÚleuon
tršcein. AÛth Ãn tîn `Rwma…wn ¹ skšyij, ¿n kaˆ prÕ crÒnwn pollîn Ãsan
meletîntej, tÕ nàn pracqšn, Óti· E„ katale…yomen tÕn K…ona toà Stauroà ™xÒpisqen ¹mîn, feuxÒmeqa tÁj melloÚshj ÑrgÁj.”–Kaˆ aÛth Ãn ¹ fug¾ tÁj
™n tÍ Meg£lV 'Ekklhs…v e„sÒdou. 'Egšneto oân ™n mi´ érv Ð Øpermegšqhj ™ke‹noj naÕj pl»rhj ¢ndrîn te kaˆ gunaikîn kaˆ k£tw kaˆ ¥nw kaˆ ™n to‹j periaÚloij kaˆ ™n pantˆ tÒpJ Ôcloj ¢nar…qmhtoj. Kle…santej d• t¦j qÚraj eƒst»kesan
t¾n par' aÙtoà swthr…an ™lp…zontej.
’W dÚsthnoi `Rwma‹oi, ð ¥qlioi, tÕn naÒn, Ön ™kale‹te cq•j kaˆ prÕ
t¾n cq•j sp»laion kaˆ bwmÕn aƒretikîn kaˆ ¥nqrwpoj oÙk e„sšrceto ™x Ømîn
™ntÒj, †na m¾ mianqÍ di¦ tÕ ƒerourgÁsai œndeon toÝj t¾n ›nwsin tÁj ™kklhs…aj
¢spazomšnouj, nàn ›neka tÁj ™pelqoÚshj ÑrgÁj æj swt»rion lÚtron ™ndÚesqe;
'All' oÙd• tÁj dika…aj ÑrgÁj ™pelqoÚshj ™k…nhsen ¨n t¦ spl£gcna Ømîn prÕj
e„r»nhn· Kaˆ g¦r ™n tosaÚtV perist£sei e„ ¥ggeloj kat»rceto ¢p' oÙranoà ™rwtîn Øm©j· E„ dšcesqe t¾n ›nwsin kaˆ t¾n e„rhnik¾n kat£stasin tÁj ™kklhs…aj,
dièxw toÝj ™cqroÝj ™k tÁj pÒlewj,” oÙk ¨n sunet…qesqe. E„ d• kaˆ sunet…qesqe,
yeàdoj ¨n Ãn tÕ suntiqšmenon. ”Isasin oƒ e„pÒntej prÕ Ñl…gwn ¹merîn· Kre‹tton ™mpese‹n e„j ce‹raj ToÚrkwn À Fr£gkwn.”
TÒte oƒ Toàrkoi kourseÚontej, sf£ttontej, a„cmalwt…zontej œfqasan
™n tù naù oÜpw prèthj éraj parelqoÚshj kaˆ eØrÒntej t¦j pÚlaj kekleismšnaj sÝn to‹j pelškesin œbalon k£tw m¾ bradÚnantej, 'ElqÒntej d• xif»reij
™ntÕj kaˆ „dÒntej tÕn muri£riqmon dÁmon, ›kastoj tÕn ‡dion a„cm£lwton ™dšsmei·
oÙ g¦r Ãn ™ke‹ Ð ¢ntilšgwn À Ð m¾ prodidoÝj ˜autÕn æj prÒbaton. T…j ™stin,
Öj dihg»setai t¾n ™ke‹ sumfor£n; T…j toÝj gegonÒtaj tÒte klauqmoÝj kaˆ t¦j
fwn¦j tîn nhp…wn kaˆ t¦ sÝn boÍ d£krua tîn mhtšrwn kaˆ tîn patšrwn toÝj
ÑdurmoÝj dihg»setai; `O tucën Toàrkoj t¾n truferwtšran ™reÚna. T¾n æra…an
™n monazoÚsaij prokate‹ce m•n eŒj, ¥lloj d• dun£sthj ¡rp£zwn ™dšsmei· ¹ d•
tÁj ¡rpagÁj kaˆ toà ˜lkusmoà a„t…a plÒkamoi tricîn, sthqšwn kaˆ masqîn
¢pokalÚyeij, braciÒnwn ™kt£seij. TÒte ™desme‹to doÚlh sÝn tÍ kur…v, despÒthj
sÝn tù ¢rgurwn»tJ, ¢rcimandr…thj sÝn tù qurwrù, truferoˆ nšoi sÝn parqšnoij, parqšnouj §j oÙc ˜èra ¼lioj, parqšnouj §j aÙtÕj Ð genn»saj mÒlij œble-

pen, ˜lkÒmenai, e„ g¦r kaˆ b…v ¢ntwqoànto, kaˆ ·abdizÒmenai. 'HboÚleto g¦r Ð
skuleÚsaj e„j tÒpon ¥gein ka…, parakataqšmenoj ™n ¢sfale…v, strafÁnai kaˆ
deutšran pra‹dan poiÁsai kaˆ tr…thn. 'Ebi£zonto oƒ ¤rpagej, oƒ ™kdikhtaˆ toà
Qeoà· kaˆ p£ntaj m•n Ãn „de‹n ™n mi´ érv desmwqšntaj, toÝj m•n ¥¸·enaj sÝn
kalwd…oij, t¦j d• guna‹kaj sÝn to‹j soudar…oij aÙtîn. Kaˆ Ãn „de‹n ÐrmaqoÝj
™xercomšnouj ¢pe…rouj ™k toà naoà kaˆ ™k tîn ¢dÚtwn toà naoà, Øp•r ¢gšlaj
kaˆ po…mnia prob£twn. Kla…ontej, ÑdurÒmenoi kaˆ Ð ™leîn oÙk Ãn.
T¦ d• toà naoà pîj; t… e‡pw À t… lal»sw; 'Ekoll»qh ¹ glîss£ mou
tù l£rugg… mou· oÙ dÚnamai ˜lkÚsai pneàma toà stÒmatÒj mou fragšntoj. AÙqwrÕn oƒ kÚnej t¦j ¡g…aj e„kÒnaj katškoyan, tÕn kÒsmon ¢felÒntej, t¦j ¡lÚseij, manou£lia, ™ndut¦j tÁj ¡g…aj trapšzhj, t¦ fwtodÒca ¢gge‹a, ¥lla fqe…rontej, ›tera lamb£nontej. T¦ toà ƒeroà skeuofulak…ou t…mia kaˆ ƒer¦ skeÚh,
crus© te kaˆ ¢rgur© kaˆ ™x ¥llhj tim…aj Ûlhj kataskeuasqšnta, ¤panta ™n
mi´ ·opÍ sun»gagon, ¢fšntej tÕn naÕn œrhmon kaˆ gumnÒn, mhdotioàn katale…yantej.

