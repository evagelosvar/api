Fragmenta in epistulam i ad Timotheum
636 1 Tim 3,16Ἀντὶ τοῦ πνευματικῶς ἐδικαιώθη ἀλλ' οὐ νομικῶς· καὶ γὰρ καὶ τὰς
νομικὰς πληρῶν ἐντολάς, οὐ νομικῶς αὐτὰς ἀλλὰ πνευματικῶς ἐξεπλήρωσεν. ἢ ὅτι
εἰ καὶ τοῖς σαρκικοῖς καὶ βαρυκαρδίοις οὐκ ἐδόκει 637 δεδικαιῶσθαι ἔλεγον γάρ·
φάγος καὶ οἰνοπότης ἀλλὰ τοῖς ἐν πνεύματι θεοῦ στηριζομένοις δεδικαίωται· καὶ
ἐθεασάμεθα γὰρ τὴν δόξαν αὐτοῦ, δόξαν ὡς μονογενοῦς καὶ ἑξῆς. 1 Tim 4,3 Οὐκ ἔστι
σφάλμα καλλιγραφικόν, ὡς ἐνίοις ἔδοξεν, οὐδὲ παρόραμα ἀποστολικόν, ἀλλ' ὀρθῶς
καὶ εἰς τὴνἈτθίδα συνήθειαν διαπεφρασμένον ὥσπερ τὸ ἐκώλυσεν αὐτὸν μὴ ποιεῖν
τὰ ἄτοπα, οὐ λέγει ὡς εἰς ἀτοπίαν αὐτὸν προὔτρεπεν· καὶ τὸ ἀπέτρεπεν αὐτὸν μὴ
προσκρούειν φίλοις, οὐχὶ φίλοις προσκρούειν ἀλλὰ τοὐναντίον· καὶ πάλιν ἐκώλυεν
μὴ κλέπτειν, οὐχ ὅτι ἐπέτρεπε τὴν κλοπήν· καὶ ἐκώλυεν ἀπέχεσθαι ἀρρητοποιΐας,
ἀντὶ τοῦ ἀπῆγεν ἀπὸ τῆς τοιαύτης πράξεως· οὕτως καὶ ἐκώλυον ἀπέχεσθαι
βρωμάτων, ἀντὶ τοῦ ἐκώλυον ἀπὸ τῆς βρώσεως. πολλὴ δὲ καὶ παρὰ τοῖς ἔξω ἡ
χρῆσις.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

