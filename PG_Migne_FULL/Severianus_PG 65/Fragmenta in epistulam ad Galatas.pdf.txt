Fragmenta in epistulam ad Galatas (in catenis)
Gal: Prolog
Τοῦ κηρύγματος εἰς τὰ ἔθνη δεχθέντος τινὲς τῶν ἀπὸ τῆς περιτομῆς ἐξιόντες
ἔπειθον τὰ ἔθνη ἰουδαΐζειν, καὶ πρώτη περὶ τούτου φαίνεται ζήτησις γεγενημένη ἐν
Ἀντιοχείᾳ, ὅτι οἱ ἀπόστολοι ἔγραψαν μὴ ὑποκεῖσθαι νόμῳ, ἀπέχεσθαι δὲ
εἰδωλοθύτου καὶ αἵματος καὶ πνικτοῦ καὶ πορνείας· οὕτω γὰρ ἔδοξε τῷ ἁγίῳ
πνεύματι. ἐκράτησε δὲ τὸ νόσημα τοῦτο τῶν Galατῶν συμφώνως ἰουδαΐζειν
βουληθέντων. ἐπείσθησαν δὲ ὑπὸ τῶν διαστρεφόντων λεγόντων, ὡς οἱ λοιποὶ
ἀπόστολοι οὐ κωλύουσι τοὺς βουλομένους τηρεῖν τὸν νόμον. 299 Gal 1,4 ∆είκνυσιν
ὅτι οὐ Μωϋσῆς ἀπέθανεν ὑπὲρ τοῦ κόσμου οὐδέ τις τῶν προφητῶν ἄλλος. οὐ δεῖ
τοίνυν ἡττᾶσθαι τοῦ μὴ σώσαντος, ἀλλὰ τοῦ ὑπὲρ ἡμῶν ἀποθανόντος. Gal 1,6–7 Εἰς
ἕτερον εὐαγγέλιον ὃ οὐκ ἔστιν ἕτερον, φησίν· τὸ γὰρ ἡμέτερον εὐαγγέλιον καὶ τὸ
τῶν ἀποστόλων ἐκτὸς ὑμᾶς εἶναι βούλεται νόμου. Gal 1,8–10 Ἔστω ἄγγελος, διὰ τί
δὲ καὶ ἡ μεῖς; ὅτε ἐκηρύττομεν, πνεύματος ἁγίου δυνάμει ἐκηρύξαμεν· τὸ δὲ πνεῦμα
τὸ ἅγιον οὐ μετανοεῖ. ἐὰν οὖν ἄλλα εἴπωμεν, κατελίπομεν τὰ τοῦ πνεύματος. ἔστω
οὖν καὶ ὁ κῆρυξ ἀνάθεμα, ἀθετῶν τὸ κήρυγμα. Παρ' ὃ παρελάβετε. οὐκ ἐκήρυξα μὲν
ἐγώ, φησίν, ὑμεῖς δὲ οὐκ ἐλάβετε, ἀλλὰ καὶ ἐγὼ ἐκήρυξα, καὶ ὑμεῖς παρελάβετε. οὔτε
ἐγὼ μετανοῶ κηρύξας, καὶ ὑμεῖς τηρήσατε ὃ εἰλήφατε. Ἄρτι γὰρ ἀνθρώπους πείθω ἢ
τὸν θεόν; ἢ ζητῶ ἀνθρώποις ἀρέσκειν; εἰ ἔτι ἀνθρώποις ἤρεσκον, Χριστοῦ δοῦλος
οὐκ ἂν ἤμην. μέλλων αὐτοὺς ὑπομιμνήσκειν τοῦ ἰδίου εὐαγγελίου, προλαβὼν
ἀσφαλίζεται ἵνα μὴ νομίσωσιν ὅτι θεραπεύει ὑπαγόμενος· ἐπιπλήττει γὰρ νουθετῶν.
εἰπὼν δὲ εἰ ἔτι, ἐδήλωσεν ὅτι ἤρεσκε ποτὲ ἀνθρώποις. τίσι δὲ ἤρεσκεν; παρ' ὧν
ἔλαβεν ἐπιστολὰς τοὺς εἰς ∆αμασκὸν πιστοὺς κακῶσαι. εἰ οὖν, φησίν, ἤρεσκον ἔτι
τοῖς Ἰουδαίοις, οὐκ ἂν ἐπίστευσα τῷ Χριστῷ. Gal 1,13 Τίς χρεία τῆς ἐν τῷ Ἰουδαϊσμῷ
ἀναστροφῆς ἀλλ' ἵνα δείξῃ, ὅτι οὐ προλήψει δουλεύει ἀλλ' ἀληθείᾳ· οὐ γὰρ μισήσας
τὸν νόμον ὑπέδραμε τὴν χάριν–ἐξεδίκει γὰρ αὐτόν–ἀλλ' εὑρὼν τὸ τέλειον ἀπέστη
τοῦ νόμου. Νῦν δείκνυσιν ὅτι πλέον τι τῶν περὶ Πέτρον ἔχει· οὐ γὰρ ἐζήλωσαν
ἐκεῖνοι ὑπὲρ τοῦ Ἰουδαϊσμοῦ ἀπειθοῦντες τῷ Χριστιανισμῷ ὡς ἐγώ. 300 μὴ τοίνυν
οἰηθῇτε ὅτι μισήσας τὸν νόμον ἦλθον εἰς τὴν χάριν– ἐξεδίκουν γὰρ τὸν νόμον–ἀλλ'
εὗρον τὸ τέλειον, καὶ οὐκ ἐξισῶ τὸν νόμον τῇ τελείᾳ χάριτι. Gal 1,16 Οὐ διαβάλλων
δὲ τοὺς ἀποστόλους τοῦτό φησιν, ἀλλ' ὅτι οὐκ ἐστοίχησα θνητῇ σαρκί. Gal 2,16 Εἰ
γὰρ ἐξ ἔργων οὐδεὶς δικαιοῦται τῶν πόνον φερόντων, ἐκ δὲ πίστεως ὅπου τὸ τῆς
εὐκολίας καὶ τὸ τῆς δικαιοσύνης μέγα, πῶς οὐκ ἀναγκαῖόν φησιν ἐάσαντας νόμον
ἀρκεῖσθαι τῇ χάριτι; ἐξ ἔργων νόμου λέγει μὴ δικαιοῦσθαι, οὐκ ἐξ ἐντολῶν· ἡ γὰρ
ἐντολὴ δικαιοῖ ἀκουομένη, τὸ δὲ ἔργον ἦν τὸ δυσκατόρθωτον. Gal 2,18 Πρὸς τὸν
Πέτρον ὁ λόγος· σὺ κατέλυσας αὐτὰ δόγματι ὅπερ ἀπεστάλη παρ' ὑμῶν τοῖς
Ἀντιοχεῦσι καὶ πάσῃ τῇ οἰκουμένῃ. ἀπρεπὲς οὖν ταύτα διδάσκειν ἑτέρους, ὧν τῆς
ἐπιμελείας ἀπέστης καὶ ἰδίῳ κατέλυσας δόγματι. Gal 2,19– Ὁ νόμος τῶν σωματικῶν
ἔχει τὴν ἐργασίαν καὶ τὴν ἀπόλαυσιν, τόδε φάγε καὶ τόδε μὴ φάγῃς, σάββατον
τήρησον, περιτομήν. ἡ χάρις οὐ βούλεταί σε διὰ τούτων εὐδοκιμεῖν. ἐνεκρώθην οὖν,
φησί, τούτοις. τί χρείαν ἔχω νόμου; Ζῶ δὲ οὐκέτι ἐγώ, ζῇ δὲ ἐν ἐμοὶ Χριστός. ὃ ἐγὼ
ἔζων ἐμαυτῷ, ὑπὸ νόμον ἤμην. ἐπειδὴ δὲ Χριστὸς ζῇ ἐν ἐμοί, ὁ μηκέτι ὑπὸ νόμον
ἀλλ' ἐν οὐρανοῖς ἐκ δεξιῶν τοῦ πατρός, οὐκ ἀθετῶ τὸν ἐν ἐμοὶ ζῶντα. Ὃ δὲ νῦν ζῶ
ἐν σαρκί, ἐν πίστει ζῶ. πιστεύω γὰρ εἰς τὸν υἱὸν τοῦ θεοῦ. εἰ δὲ ὁ υἱὸς τοῦ θεοῦ ᾧ
πιστεύω, ἐν δεξιᾷ τοῦ πατρός, ἐν οὐρανοῖς πολιτεύομαι· ὁ δὲ νόμος ἐν τοῖς ἐπὶ γῆς
εἶχε τὰς τηρήσεις. οὐκ ἄρα ὑπόκειμαι νόμῳ. 301 Gal 3,19–22 Ἐβάρει πλέον ὁ νόμος,
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

1

καὶ διακονία θανάτου διὰ τοῦτο ὠνόμασται. πῶς οὖν τῶν παραβάσεων χάριν
προσετέθη, φησίν; ἐν συντόμῳ ῥήματι τοιοῦτόν ἐστιν· πρὸ νόμου καὶ ὁ νοῦς τῶν
ἀνθρώπων μετὰ τῆς σαρκὸς ἐδούλευσε τῇ ἁμαρτίᾳ καὶ ἔστεργε τὸ κακόν, δοθέντος
τοῦ νόμου ὁ μὲν νοῦς ἐπαιδεύθη, ἡ δὲ σὰρξ ἔτι κατείχετο. ἐπεὶ οὖν ὁ μὲν ἀγνοῶν τὴν
ἁμαρτίαν ὅτι ἁμαρτία ἦν, τὴν ἀσέβειαν ὡς εὐσέβειαν μετιὼν καὶ σπουδαιότερον
αὐτῇ προσέχει, ὁ δὲ γνοὺς ὅτι κακόν, τοῦτο γοῦν κερδανεῖ τὸ συνειδέναι ὅτι
παραβαίνει, καὶ ἐπιθυμεῖ τῷ θεῷ ὑπακοῦσαι. διὰ τοῦτο ἐδόθη νόμος ὁ δυνάμενος
ζωοποιῆσαι, ἀντὶ τοῦ εἰ ἐδόθη νόμος, οὐκ ἐδόθη, φησί, κατὰ τῶν ἐπαγγελιῶν τοῦ
θεοῦ, ἀλλ' οὐδὲ διὰ παντὸς κρατεῖν ἐδύνατο. Gal 3,23 Εἰ καὶ μηδὲν ἕτερον κατώρθου
ὁ νόμος, ἀλλὰ τὸ τὸν θεὸν εἰδέναι παρεῖχεν. τὸ δὲ τὸν θεὸν εἰδέναι ἕτοιμον ἐποίει
πρὸς τὴν τῆς χάριτος ὑποδοχήν. Gal 3,27–28 Οὐχ ἡ τοῦ νόμου τήρησις, φησί,
βελτίονας ποιεῖ τοὺς πιστεύσαντας τῶν ἀπὸ τῶν ἐθνῶν τοὺς Ἰουδαίους, ἀλλ' ἡ
δύναμις τοῦ βαπτίσματος τὸ Χριστὸν ἐνδύσασθαι παρέχουσα, κοινὴν ποιεῖ τὴν χάριν
πάντων ὁμοῦ, βαρβάρων, Ἑλλήνων, Ἰουδαίων, θήλεος, ἄρρενος. οὐχ ἁπλῶς δὲ
παρέμιξε τὸ θῆλυ καὶ τὸ ἄρρεν–ὅσον γὰρ εἷς τῷ ῥήματι ἐζήτει πρὸς τὸ προκείμενον–,
ἀλλ' εἰ τὸ ἄρρεν καὶ τὸ θῆλυ διαφέρον εἰς μίαν χάριν συνάγει τὸ βάπτισμα, πόσῳ
μᾶλλον Ἕλληνα καὶ Ἰουδαῖον. Gal 4,8–10 Νῦν βούλεται δεῖξαι ὅτι μετὰ τὸ τὴν χάριν
ἐλθεῖν, κἂν δοκῇ τις κατὰ νόμον σάββατον τηρεῖν καὶ νεομηνίας καὶ ἑορτάς, ὥσπερ
εἰδωλολατρεῖ τοῖς στοιχείοις οἷς ἐθρήσκευεν ἐθνικὸς ὤν, ἄλλως δουλεύων. καὶ οὐ
νόμον τηρεῖ· πέπαυται γάρ. πτωχὰ δὲ στοιχεῖα εἰπὼν οὐ κατηγόρησεν ὡς πονηρῶν,
ἀλλ' ὡς πλουτίζειν μὴ δυναμένων καὶ ἀρκεῖν πρὸς εὐσέβειαν. 302 Gal 4,12 Μὴ
νομίσητε, φησίν, ὅτι ἄλλην τινὰ λύπην εἶχον πρὸς ὑμᾶς καὶ διὰ τοῦτο ἐπιπλήττω·
τῆς γὰρ ὀδύνης τὸ πάθος συνεχῶς αὐτὸν μεταβάλλειν ἀναγκάζει τοῦ λόγου τὸ εἶδος,
καὶ νῦν μὲν ἐπιτιμᾶν, νῦν δὲ παρακαλεῖν, καὶ ποτὲ μὲν κατηγορεῖν, ποτὲ δὲ θρηνεῖν.
ἐνταῦθα τοίνυν διδάσκει ὡς ἅπερ γράφει, διὰ φιλοστοργίαν γράφει–οὐδὲν γάρ, φησί,
παρ' ὑμῶν ἠδίκημαι–, ἀλλὰ καὶ θεραπείαν, ὅτι μάλιστα πλείστης ἠξίωμαι. Gal 4,15–
18 Τίς οὖν ὁ μακαρισμός; ἀντὶ τοῦ οὐδείς. ἢ οὕτως· τὸ τηρεῖν νόμον ἢ τὸ πίστει ζῆν;
καὶ ἐπειδὴ εἶπεν ὅτι ἐκεῖνοι θέλουσιν οὐχὶ ὑμᾶς ζηλοῦν, ἵνα τὸ τέλειον αὐτοὶ ἔχωσι
τῆς πίστεως μὴ τηροῦντες νόμον, ἀλλ' αὐτοὺς ὑμᾶς ζηλοῦν, ἐπήγαγεν ὅτι ταῦτα
λέγω οὐ φθονῶν αὐτοῖς· ὤφελον γὰρ κἀκεῖνοι τέλειοι ἦσαν τῇ πίστει κεχρημένοι,
καὶ ἀκώλυτον ἦν αὐτοὺς ζηλοῦσθαι καὶ μιμεῖσθαι ἡμᾶς. καὶ ὑμεῖς οὖν τοιαῦτα
πράττετε ἃ καὶ ζηλωτοὺς ὑμᾶς ποιεῖ, ἵνα πάντες ὑμᾶς μιμῶνται, καὶ μὴ ὅταν ἔλθω
αἰδεσθέντες με τοιοῦτοι γίνεσθε, ἀλλὰ καὶ ἐμοῦ μὴ παρόντος ἑαυτοὺς διορθοῦσθε.
Gal 4,23–24 Νῦν τῷ ὀνόματι κατεχρήσατο, οὐ τῇ δυνάμει τῆς ἀλληγορίας. ἡ γὰρ
ἀλληγορία οὐ στοιχεῖ τοῖς ῥητοῖς, ἄλλα δέ τινα ἐμφαινόμενα ἀπὸ τῆς κατὰ τὴν ὑφὴν
τῶν νοημάτων ἀκολουθίας εἰσάγει, οἷόν ἐστι τὸ φιλησάτω με ἀπὸ φιλημάτων
στόματος αὐτοῦ, τοῦ ἐν τοῖς Ἄσμασι τῶν Ἀσμάτων· ἐκεῖ γὰρ οὔτε φίλημα οὔτε θρὶξ
οὔτε κοιλία οὐδὲ μηροὶ οὐδὲ ἕτερόν τι, ἀλλ' ἄλλα δι' ἄλλων εἰσάγεται, καὶ τοῦτό
ἐστιν ἀλληγορίας εἶδος κύριον. ἐνταῦθα δὲ καὶ ἡ ἱστορία ὡμολόγηται, καὶ ὅτι τύπος
ἐστὶ τοῦ μέλλοντος ἀποδέδεικται. ἡ οὖν δι' ἔργων προφητεία ἀλληγορία νῦν
ὠνόμασται. τῶν γὰρ προφητειῶν αἱ μέν εἰσι διὰ λόγων μόνον ὡς τὸ ἰδοὺ ἡ παρθένος
ἐν γαστρὶ λήψεται, αἱ δὲ δι' ἔργων μόνον ὡς Μωϋσῆς ὕψωσε τὸν ὄφιν ἐν τῇ ἐρήμῳ,
αἱ δὲ καὶ δι' ἔργων καὶ διὰ λόγων ὡς τὸ πρὸς 303 Ἰερεμίαν ῥηθέν, τὸ λάβε περίζωμα
καινὸν καὶ περίθου· εἶτα θάψον αὐτὸ παρὰ τὸν Εὐφράτην, καὶ ὡς διερρύη τῷ χρόνῳ
καὶ διεβρώθη, λάβε αὐτό, φησίν, ἐκεῖθεν. ἕως γὰρ τούτου δι' ἔργων ἡ προφητεία, τὸ
δὲ ἑξῆς καὶ διὰ λόγων· ὅτι οὕτως κἀγὼ τοὺς υἱοὺς Ἰσραὴλ περιεθέμην καὶ οὕτως
αὐτοῖς ἐβοήθησα, καὶ οὕτως αὐτοὺς εἰς Βαβυλῶνα ἀπάγω αἰχμαλώτους καὶ
σαθρωθέντας καὶ φθαρέντας ἐκεῖθεν ἀναγάγω καὶ ἀποκαθιστῶ. νῦν οὖν τὴν δι'
ἔργων προφητείαν ἀλληγορίαν ὠνόμασεν. Gal 4,25 Οὐχ ἁπλῶς ἐμνημόνευσε τοῦ
 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

2

Σινᾶ ὄρους, ἀλλ' ἐπειδὴ οἱ Ἀγαρηνοὶ οἱ ἀπὸ τοῦ Ἰσμαὴλ τὴν ἔρημον ἔχουσι τὴν
παρατείνουσαν ἕως τοῦ Σινᾶ ὄρους. ἐν δὲ τῷ Σινᾷ ὁ νόμος ἐδόθη εἰς δουλείαν
γεννῶν, ἀπὸ δὲ τοῦ Σινᾶ ὄρους αἱ πλάκες δοθεῖσαι ἐκομίσθησαν ἐν τῷ Ἰσραήλ, καὶ
πολλάκις μετενεχθεῖσαι ἀπὸ τόπου εἰς τόπον τελευταῖον ἱδρύθησαν ἐν
Ἱεροσολύμοις. διὰ τοῦτο ἐπήγαγεν· συστοιχεῖ δὲ τῇ νῦν Ἱερουσαλήμ, δουλεύει δὲ
μετὰ τῶν τέκνων αὐτῆς. ὅπου γῆς ἐπαγγελία, καὶ κτῆσις καὶ γάμος καὶ τὰ
ἀνθρώπινα, καὶ δουλεία ἀκολουθεῖ· ὅπου δὲ ἀπαλλαγὴ τῶν ἐπὶ γῆς φροντίδων καὶ
τῶν δεσμῶν, καὶ ἐν οὐρανοῖς ἡ πολιτεία, ἐξ αὐτῆς τῆς πολιτείας ἡ ἐλευθερία. Gal
5,17 Ζητεῖται διὰ τί νόμου ἀποσπῶν μέμνηται τῆς ἁμαρτίας. ἐπειδὴ τρυφᾶν ἐκέλευεν
ὁ νόμος, διὰ δὲ τούτου διερεθίζεται ἡ ἐπιθυμία καὶ ἀκατάσχετος οὖσα πρὸς ἁμαρτίαν
ἄγει. τούτου ἕνεκεν ὅτε νόμου ἦν μνήμη, μέμνηται καὶ τῆς ἁμαρτίας. καὶ γὰρ ἐν τῇ
πρὸς Ῥωμαίους τοῦ νόμου μνημονεύων καὶ τῆς διὰ τοῦ κυρίου διορθώσεως
εἰσήγαγεν, τὸ οἱ γὰρ κατὰ σάρκα ζῶντες θεῷ ἀρέσαι οὐ δύνανται. διὰ τί δὲ
συνεχωρήθη τὸ τρυφᾶν καὶ ἐσθίειν; ὅτι ἡ τῶν ὡμολογημένων κακῶν ἀποχὴ οὐκ ἂν
προεχώρησεν, εἰ μὴ ἐνεδόθη αὐτοῖς τὸ ἐσθίειν 304 καὶ πίνειν· ᾔδει μὲν γὰρ ὁ
νομοθέτης ὅτι ἀδύνατον τρυφῶντας κρατεῖν ἡδονῶν. τέως δὲ ἠθέλησε τὸν νοῦν
παιδεῦσαι, ἵνα γνῷ τί καλὸν καὶ τί κακὸν καὶ τί τὸ εὐσεβές· προεώρα γὰρ ὅτι ἔμελλεν
ἔρχεσθαι ὁ τὸ λεῖπον τῷ νόμῳ προστιθείς, λέγω δὴ τὸ μὴ κατὰ σάρκα περιπατεῖν, ὡς
λέγει ὁ Παῦλος. Gal 6,7–8 Ἐδήλωσεν ἐκ τούτου ὅτι πολλοὶ ἀκαίρως μὲν ἀνήλισκον,
παρέχοντες τοῖς τηρεῖν νόμον παρακελευομένοις, τοῖς δὲ τὴν Παύλου διδασκαλίαν
κηρύττουσιν οὐδὲν παρεῖχον. διό φησιν· ὁ σπείρων εἰς τὴν σάρκα αὐτοῦ· ὁ γὰρ νόμῳ
ὑποκείμενος ἐν τοῖς σαρκικοῖς ζῇ. ὁ τοίνυν τῷ ταῦτα διδάσκοντι παρέχων εἰς τὴν
σάρκα σπείρει μόνον. Gal 6,14–15 Ἵνα μὴ νομισθῇ ἀκροβυστίας ἀντιποιεῖσθαι, καὶ
οὐχὶ περιτομῆς καὶ διαβολῆς πρόφασιν δῷ τοῖς ἀντιλέγουσιν, κάλλιστα ἐπήγαγε τὸ
τελευταῖον, ὅτι οὔτε περιτομή τι ἰσχύει οὔτε ἀκροβυστία, καινὴν δὲ κτίσιν εἶπεν.
περὶ ἧς ἀλλαχοῦ· εἴ τις ἐν Χριστῷ, καινὴ κτίσις. εἰ καινή, φησί, κτίσις, μὴ γίνου ὑπὸ
νόμον· ὁ γὰρ ζῶν κατὰ νόμον ἐν κόσμῳ ζῇ, ὁ δὲ ζῶν ἐν Χριστῷ, ἐν οὐρανοῖς
πολιτεύεται. ἐσταύρωται οὖν ἐμοὶ κόσμος κἀγὼ κόσμῳ.

 
 

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

3

