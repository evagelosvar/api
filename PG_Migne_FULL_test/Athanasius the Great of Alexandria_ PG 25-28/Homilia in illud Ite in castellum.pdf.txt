Homilia in illud: Ite in castellum
Τοῦ αὐτοῦ Ἀθανασίου, ἀρχιεπισκόπου Ἀλεξανδρείας, εἰς τό· «πορεύεσθε
εἰς τὴν κατέναντι κώμην καὶ εὑρήσετε ὄνον δεδεμένην καὶ πῶλον μετ'
αὐτῆς· λύσαντες ἀγάγετέ μοι.»
1.1 Πάλιν ἡμᾶς εὐεργετῶν ὁ κύριος καὶ τὴν οἰκείαν ἑαυτοῦ εἰς ἡμᾶς ἐκτείνων χάριν
ὁμοῦ καὶ διὰ τῆς ἑαυτοῦ διδασκαλίας τὰς ἡμετέρας διεγείρει ψυχάς. 1.2 τοῖς γὰρ ἐν
τῷ βίῳ τούτῳ μοχθήσασι καὶ περισπασθεῖσιν ἐν τοῖς αὐτοῦ πράγμασιν ὁ θεὸς τὸν
αἰῶνα τοῦτον ἐν καρδίᾳ δέδωκεν, ὅπως γνόντες γνώσονται οἱ ἄνθρωποι, ὅτι οὐ τοὺς
αἰῶνας διαμένουσιν, ἀλλὰ τὸν αἰῶνα τοῦτον φαντάζονται. 1.3 ἵνα οὖν καὶ τοῦ
μέλλοντος αἰῶνος πρόνοιαν ποιή σωνται, ἔμφυτον ἡμῖν μάθησιν διὰ τοῦ
αὐτεξουσίου ἐχαρίσατο. πάντες οὖν οἱ ἄνθρωποι σοφοί τε καὶ ἄφρονες, ἄρχοντες καὶ
ἰδιῶται, πλούσιοι καὶ πένητες, ἴσασιν, ὅτι θάνατος πάντας διαδέξεται. 1.4 τοῦτο γάρ
ἐστι τὸ τὸν αἰῶνα τοῦτον φαντάζεσθαι, τουτέστιν ἕκαστον τὸ ἑαυτοῦ τέλος πρὸ
ὀφθαλμῶν ἔχειν. 1.5 πολλοὶ δὲ καὶ τοῦτο εἰδότες, ὅτι θάνατος αὐτοὺς διαδέξεται,
οἴονται μετὰ τοῦτον τὸν αἰῶνα ἔσεσθαι ὡς οὐχ ὑπάρξαντες. 1.6 οἷοι ἦσαν οἱ
εἰπόντες· «ὀλίγος ἐστὶ καὶ λυπηρὸς ὁ βίος ἡμῶν καὶ οὐκ ἔστιν ἀναποδισμὸς τῆς
τελευτῆς ἡμῶν. δεῦτε οὖν,» φησί, «καὶ ἀπολαύσω μεν τῶν ὄντων ἀγαθῶν· οἴνου
πολυτελοῦς καὶ μύρων ἐμπλησθῶμεν.» 1.7 τούτους δὲ καὶ ὁ θεσπέσιος Παῦλος
διατίθησι λέγοντας· «φάγωμεν καὶ πίωμεν, αὔριον γὰρ ἀποθνῄσκομεν.» 1.8 εἰ
τοσοῦτον οὖν ἴσχυσαν γνῶναι οἱ ἄθλιοι, ὅτι «ὀλίγος ἐστὶ καὶ λυπηρὸς ὁ βίος»
αὐτῶν, πῶς τὸν τῶν ἀγαθῶν ποιητὴν τάχιον οὐχ εὗρον; 1.9 μόνον οὖν σοφοῦ ἐστι
γνῶναι τὰς αἰτίας τῶν γενομένων. οἶδε γὰρ ὁ σοφὸς καὶ πῶς ἐγένοντο καὶ οὗ χάριν
ἐγένοντο. οἶδεν ὅτι τῶν πάντων ὁ θεὸς ποιητής ἐστιν. 1.10 ἤκουσε γὰρ τοῦ
ἀποστόλου λέγοντος· «εἴτε ἐσθίετε εἴτε πίνετε εἴτε τι ποιεῖτε, πάντα εἰς δόξαν θεοῦ
ποιεῖτε.» 1.11 οἱ δὲ τῷ βίῳ τούτῳ χρησάμενοι ὡς ἐν ἡμέραις νεότητος σπουδαίως καὶ
τῇ ἑαυτῶν κακίᾳ καταδαπανηθέντες, ἀπ' ἀρχῆς μέχρι τέλους οὐκ ἔγνωσαν τὰ
ποιήματα τοῦ θεοῦ. εἰ γὰρ ἦσαν ἀκριβώσαντες περὶ τούτων τὸν νοῦν πάντως ἂν καὶ
τὸν τούτων δημιουργὸν θεὸν τάχιον ἐπεγίνωσκον. 1.12 «ἐκ γὰρ μεγέθους καὶ
καλλονῆς κτισμάτων ὁ γενεσιουργὸς αὐτῶν θεωρεῖται.» ὁδῷ οὖν κέχρηται ἀγούσῃ
τοὺς ἀνθρώπους ἐκ τῶν κτισμάτων ἐπὶ τὸν τῶν ἁπάντων κτίστην καὶ δημιουργόν.
2.1 Ταῦτα μὲν προσεθήκαμεν τῷ λόγῳ, ἐπειδήπερ πάντες ὡς πρόβατα ἐπλανήθησαν
καὶ ὅτι «φθόνῳ διαβόλου θάνατος εἰσῆλθεν εἰς τὸν κόσμον» ὅτι «σειραῖς τῶν
ἑαυτοῦ ἁμαρτιῶν ἕκαστος ἐσφίγγετο.» 2.2 «ἐβασίλευσε» γὰρ «ὁ θάνατος ἀπὸ Ἀδὰμ
μέχρι Μωϋσέως καὶ ἐπὶ τοὺς μὴ ἁμαρ τήσαντας ἐπὶ τῷ ὁμοιώματι τῆς παραβάσεως
Ἀδάμ.» 2.3 ἀλλ' ἐπεδή μησεν ὁ κύριος καὶ σωτὴρ ἡμῶν Ἰησοῦς ὁ χριστὸς
λυτρούμενος τοὺς αἰχμαλώτους καὶ ζῳοποιῶν τοὺς τεθανατωμένους. 2.4 ἀμέλει καὶ
ὁ θεσπέσιος ἀπόστολος τοῦτο ἐπιστάμενος ἔγραφε λέγων· «εἰ γὰρ τῷ τοῦ ἑνὸς
παραπτώματι οἱ πολλοὶ ἀπέθανον, πολλῷ μᾶλλον τῷ τοῦ ἑνὸς δικαιώματι εἰς
πάντας ἀνθρώπους εἰς δικαίωσιν ζωῆς.» 2.5 ἐπεδήμησε τοιγαροῦν πρὸς ἡμᾶς ὁ ζῶν
καὶ ἐνεργὴς τοῦ θεοῦ λόγος, ἡ εἰκὼν ἡ ἀπαράλλακτος, ὁ υἱὸς ὁ μονογενής, «δι' οὗ
γεγόνασιν οἱ αἰῶνες, φέρων τε τὰ πάντα τῷ ῥήματι τῆς δυνάμεως αὐτοῦ,
καθαρισμὸν τῶν ἁμαρτιῶν ποιησάμενος.» 2.6 διὸ ἀποστέλλει δύο μαθητὰς εἰς τὴν
κατέναντι κώμην λέγων πρὸς αὐτούς· «πορεύθητε εἰς τὴν κώμην τὴν κατέναντι
ὑμῶν, καὶ εὐθέως εὑρήσετε ὄνον δεδεμένην καὶ πῶλον μετ' αὐτῆς· λύσαντες
 
 
 
 

1

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

ἀγάγετέ μοι.» 2.7 Ματθαῖος μὲν διαγράφων τὴν ἐκκλησίαν καὶ τὰ ἐξ αὐτῆς γενόμενα
ὄνον καὶ πῶλον ὠνόμασε. Λουκᾶς δὲ τὰ ἀμφότερα εἰς μίαν ἑνότητα
ἀνακεφαλαιούμενος πῶλον ὀνομάζει. 2.8 φησὶ γὰρ οὕτω Λουκᾶς ἐν τῷ καθ' ἑαυτὸν
εὐαγγελίῳ· «ἀπέστειλε δύο τῶν μαθητῶν αὐτοῦ εἰπών· ὑπάγετε εἰς τὴν κατέναντι
κώμην, ἐν ᾗ εἰσπορευόμενοι εὑρήσετε πῶλον δεδεμένον, ἐφ' ὃν οὐδεῖς πώποτε
ἀνθρώπων ἐκάθισε· λύσαντες ἀγάγετέ μοι.» 2.9 εὑρήσεις οὖν τὸν μὲν Ματθαῖον
πανταχοῦ δύο ὀνομάζοντα, τὸν δὲ Λουκᾶν εἰς ἓν τὰ δύο ἀνακεφαλαιοῦντα. 2.10 οἷον
καὶ ἐπὶ τῶν τυφλῶν ὁ Ματθαῖος πάλιν δύο ὀνομάζει εἰπών· «καὶ παράγοντι ἐκεῖθεν
τῷ Ἰησοῦ ἠκολούθησαν αὐτῷ δύο τυφλοὶ κράζοντες καὶ λέγοντες· ἐλέησον ἡμᾶς,
υἱὸς ∆αυίδ.» 2.11 ὁ δὲ Λουκᾶς τοὺς δύο εἰς ἕνα ἀνακεφαλαιούμενός φησιν· «ἐγένετο
δὲ ἐν τῷ ἐγγίζειν αὐτὸν εἰς Ἰεριχὼ τυφλός τις ἐκάθητο παρὰ τὴν ὁδὸν προσαιτῶν.
ἀκούσας δὲ ὄχλου διαπο ρευομένου ἐπυνθάνετο τί ἂν εἴη τοῦτο. ἀπήγγειλαν δὲ αὐτῷ
ὅτι Ἰησοῦς ὁ Ναζωραῖος παρέρχεται. καὶ ἐβόησε λέγων· Ἰησοῦ υἱὲ ∆αυίδ, ἐλέησόν
με.» 2.12 ὁμοίως καὶ ἐπὶ τῶν δαιμονιζομένων εὑρήσεις τὸν μὲν Ματθαῖον δύο
ὀνομάζοντα, τὸν δὲ Λουκᾶν ἀκριβεστέρᾳ τῇ διανοίᾳ εὐσεβῶς ἀντὶ τῶν δύο ἕνα
εἰρηκότα. 2.13 φησὶν οὖν ὁ Ματθαῖος περὶ τῶν δύο οὕτω· «καὶ ἐλθόντι αὐτῷ εἰς τὸ
πέραν εἰς τὴν χώραν τῶν Γεργεσηνῶν ὑπήν τησαν αὐτῷ δύο δαιμονιζόμενοι ἐκ τῶν
μνημείων ἐξερχόμενοι, χαλεποὶ λίαν.» 2.14 Λουκᾶς δὲ τοὺς δύο εἰς ἕνα γράφων
ἔλεγεν· «ἐξελθόντι δὲ αὐτῷ ἐπὶ τὴν γῆν ὑπήντησεν ἀνήρ τις ἐκ τῆς πόλεως, ὃς εἶχε
δαιμόνια ἐκ χρόνων ἱκανῶν.» 2.15 πανταχοῦ οὖν εὑρήσεις τὸν μὲν Ματθαῖον δισσῶς
περὶ πάντων φθεγγόμενον, τὸν δὲ Λουκᾶν ἑνικῶς διαλεγόμενον. 2.16 ὥστε οὐκ
ἐσφαλμένα εἰσὶ τὰ θεῖα εὐαγγέλια–μὴ γένοιτο–οὔτε πάλιν ἀντίθετα ἑαυτοῖς
ἐφθέγξαντο οἱ ἐξ ἑνὸς τοῦ ἁγίου πνεύματος ἐμφορούμενοι· μίαν γὰρ καὶ τὴν αὐτὴν
σῴζουσι διὰ τῶν ἁγίων γραφῶν δύναμιν οἱ ἅγιοι. 3.1 Τούτων δὲ καὶ ἡμεῖς
ἐμνημονεύσαμεν οὐκ ἐκπεσόντες τοῦ προκειμένου. ἀνεγνώσθη γὰρ ἡμῖν, ὡς
ἠκούσατε, εὐαγγέλιον τὸ κατὰ Ματ θαῖον. 3.2 ἡ δὲ περιοχὴ τῆς γραφῆς ἐστιν αὕτη·
«τότε ὁ Ἰησοῦς ἀπέστειλε δύο μαθητὰς λέγων αὐτοῖς· πορεύθητε εἰς τὴν κώμην τὴν
κατέναντι ὑμῶν καὶ εὐθέως εὑρήσετε ὄνον δεδεμένην καὶ πῶλον μετ' αὐτῆς·
λύσαντες ἀγάγετέ μοι.» 3.3 ἤδη προειρήκαμεν, ὅτι αὕτη ἡ ὄνος ἡ ἐξ ἐθνῶν ἐκκλησία
ἐτύγχανεν, ὁ δὲ πῶλος ὁ πατὴρ ἡμῶν Ἀδάμ ἐστιν, ὃν ἔδησεν ὁ σατανᾶς ταῖς ἑαυτοῦ
πανουργίαις. 3.4 αὕτη γὰρ ἡ ὄνος ἀκάθαρτον ζῷον ἐχρημάτιζε, μὴ ἀνάγουσα μήτε
κατάγουσα μηρυκισμόν. ἥτις ἐστὶν ἡ ἐξ ἐθνῶν ἐκκλησία. 3.5 ἐμιαίνετο γὰρ πρὸ
τούτου ἐν τοῖς εἰδώλοις αὐτῆς καὶ ἐμολύνετο ἐν τοῖς ἀκαθάρτοις αἵμασιν. ὡς
λέγεσθαι πρὸς αὐτὴν διὰ Ἠσαΐου τοῦ προφήτου· «ἐμοιχεύσω ἐν ποιμέσι πολλοῖς.»
3.6 ἀλλ' ὁ φιλάνθρωπος τοῦ θεοῦ υἱὸς ἐπεδήμησε πρὸς ἡμᾶς «καὶ σχήματι εὑρεθεὶς
ὡς ἄνθρωπος ἐταπείνωσεν ἑαυτὸν γενόμενος ὑπήκοος μέχρι θανάτου, θανάτου δὲ
σταυροῦ.» 3.7 οὐ γὰρ ἦν ἀγγέλων τὸ σῶσαι κτίσιν θεοῦ ἀλλὰ λόγου θεοῦ, θεοῦ τοῦ
καταβάντος «μεσίτου θεοῦ καὶ ἀνθρώπων.» αὐτῷ γὰρ καὶ ἐχρεωστεῖτο τῷ καὶ κτίστῃ
εἰρημένῳ. 3.8 ἀποστέλλει οὖν τοὺς δύο μαθητὰς εἰς τὴν κατέναντι κώμην λῦσαι τὴν
ὄνον καὶ τὸν πῶλον, «ἵνα παραστήσῃ αὐτὸς ἑαυτῷ ἔνδοξον τὴν ἐκκλησίαν, μηκέτι
ἔχουσαν σπίλον ἢ ῥυτίδα» ἢ ἄλλο τι τῶν προλεχθέντων περὶ αὐτῆς, «ἀλλ' ἵνα ᾖ ἁγία
καὶ ἄμωμος» τῷ Χριστῷ. 3.9 οὕτω γὰρ καὶ τὰ ἐξ αὐτῆς γεννώμενα ἔσονται εἰς «λαὸν
περιούσιον, ζηλωτὴν καλῶν ἔργων.» 4.1 Ἀπέρχονται οὖν οἱ δύο μαθηταὶ εἰς τὴν
κατέναντι κώμην καὶ λύουσι τὴν ὄνον καὶ τὸν πῶλον κατὰ τὰ κελευσθέντα αὐτοῖς
παρὰ τοῦ διδασκάλου. 4.2 ἀνεκαλέσατο γὰρ ἡμᾶς ἐκ τῆς κατέναντι κώμης εἰς τὴν
ἐπουράνιον πόλιν, τὴν ἄνω Ἰερουσαλήμ. 4.3 ἔλυσε δὲ ἡμᾶς ἐκ τῶν τοῦ διαβόλου
δεσμῶν. οἶδας καὶ τὸν προφήτην Ἠσαΐαν πρὸ πολλοῦ βοᾶν καὶ λέγειν· «ἔδωκά σε εἰς
 
 
 
 

2

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

διαθήκην γένους μου, εἰς φῶς ἐθνῶν ἀνοῖξαι ὀφθαλμοὺς τυφλῶν, ἐξαγαγεῖν ἐκ
δεσμῶν ἀνειμένους καὶ ἐξ οἴκου φυλακῆς καθημένους ἐν σκότει.» 4.4 πορεύονται
οὖν οἱ μαθηταὶ εἰς τὴν κατέναντι κώμην. κώμην δὲ ὅταν ἀκούσῃς τὸν περίγειον
τοῦτον κόσμον ὑπολάμβανε· ἡ γὰρ πόλις ἡ ἐπουράνιός ἐστιν ὡς προείπαμεν. 4.5 τὸ
δὲ καὶ "3εἰς τὴν κατέναντι κώμην"3 Μωϋσῆς ἐν τῇ Γενέσει προεσήμανεν εἰπών·
«ἀπῴκησεν ὁ θεὸς τὸν Ἀδὰμ ἀπέναντι τοῦ παραδείσου τῆς τρυφῆς καὶ ἐκέλευσε τὰ
χερουβὶμ καὶ τὴν φλογίνην ῥομφαίαν τὴν στρεφομένην φυλάττειν τὴν ὁδὸν τοῦ
ξύλου τῆς ζωῆς.» 4.6 οὐδενὸς οὖν ἄλλου ἦν εἰσαγαγεῖν ἡμᾶς, ὅθεν ἐξεβλήθημεν, ἢ
αὐτοῦ τοῦ κυρίου τοῦ τεθεικότος τὰ χερουβὶμ φυλάττειν τὴν ὁδὸν τοῦ ξύλου τῆς
ζωῆς. 4.7 οὐδενὸς γὰρ ἄλλου ἦν ἀνακαλέσασθαι ἡμᾶς ἐκ τῆς κατέναντι κώμης εἰς
τὴν ἐπουράνιον πόλιν ἢ αὐτοῦ τοῦ ἀποφηναμένου καθ' ἡμῶν. 4.8 οὕτως οὖν
νοηθήσεται καὶ τὸ ἐπὶ τῆς ὄνου καὶ τοῦ πώλου, ὅτι πᾶσα ἡ ἀνθρωπότης ἐστίν.
ὑποδείγμασι γὰρ κέχρηται ἡ θεία γραφή. 4.9 κἂν γὰρ τὴν ἐκκλησίαν διαλάβω, τὸν
ἄνθρωπον σημαίνω· κἂν τοῦ Ἀδὰμ μνήμην ποιήσωμαι, ἀλλὰ πᾶσαν ὁμοῦ τὴν
ἀνθρωπότητα ὁμολογῶ. 4.10 διὰ τοῦτο γὰρ ἐπεδήμησεν ὁ κύριος καὶ σωτὴρ ἡμῶν
Ἰησοῦς ὁ χριστός, ἵνα τὸ πεπλανημένον ἐπιστρέψῃ καὶ τὸ ἀπολωλὸς ζητήσας εὕρῃ.
4.11 αὐτὸς γάρ ἐστιν «ὁ λύων τοὺς πεπεδημένους καὶ ἀνορθῶν τοὺς
κατερραγμένους» καὶ ἀνακαλούμενος τοὺς ἐκπεσόντας καὶ λυτρούμενος τοὺς
αἰχμαλώτους. 4.12 τοῦτο δὲ μηνύων καὶ ὁ θεόφορος ∆αυὶδ ἐν ψαλμοῖς ᾄδει λέγων·
«ἐπίτρεψον, κύριε, τὴν αἰχμαλωσίαν ἡμῶν ὡς χειμάρρους ἐν τῷ νότῳ.» 4.13 οὗτός
ἐστιν ὁ καὶ τὸ σκότος καταλάμπων, περὶ οὗ γέγραπται· «Γαλιλαία τῶν ἐθνῶν, ὁ λαὸς
ὁ καθήμενος ἐν σκότει, ἴδετε φῶς μέγα.» 4.14 τῆς γὰρ παρουσίας τοῦ σωτῆρος
καταλαμψάσης τὰ σύμπαντα λύεται λοιπὸν ἡ ὄνος καὶ ὁ πῶλος ἐκ τῶν δεσμῶν. 4.15
αὐτὸς γὰρ καὶ τὰς ἁμαρτίας ἡμῶν ἦρεν, ὥς φησι διὰ τοῦ προφήτου· «ἐγώ εἰμι ὁ
ἐξαλείφων τὰς ἁμαρτίας σου.» 5.1 ∆είξωμεν οὖν καὶ ἡμεῖς ἀγαθὸν τῇ ψυχῇ ἑαυτῶν.
λύσωμεν πάντα σύνδεσμον ἀδικίας. τοῦτο γάρ ἐστι τὸ κυρίως ἀγαθόν, ἵνα, ἐν οἷς
αὐτὸς ὁ διδάσκαλος ὀρθῶς καὶ καλῶς ἐπολιτεύσατο, μανθάνωμεν αὐτὸν μὴ λόγῳ
μόνον παραδεδωκέναι ἡμῖν, ἀλλὰ καὶ πράγμασι τὰ συμφέροντα. 5.2 ἄκουε γὰρ αὐτοῦ
λέγοντος διὰ τοῦ προφήτου· «πνεῦμα κυρίου ἐπ' ἐμέ, οὗ ἕνεκεν ἔχρισέ με·
εὐαγγελίσασθαι πτωχοῖς ἀπέσταλκέ με, ἰάσασθαι τοὺς συντετριμμένους τῇ καρδίᾳ,
κηρύξαι αἰχμαλώτοις ἄφεσιν καὶ τυφλοῖς ἀνάβλεψιν.» 5.3 δήσαντες γὰρ ἡμῶν πόδας
καὶ χεῖρας οἱ πρὸ τούτου κύριοι ᾐχμαλώτευσαν ἡμᾶς προκόπτειν οὐκ ἐπιτρέποντες
τῇ ὁδῷ τῆς ἀληθείας. 5.4 ἀλλ' ὁ φιλάνθρωπος τοῦ θεοῦ υἱὸς ἐπιδημήσας τῷ γένει τῷ
ἀνθρωπίνῳ ἠλευθέρωσεν ἡμᾶς. ἀπέστειλε γὰρ τοὺς ἑαυτοῦ μαθητὰς καὶ ἔλυσεν ἡμᾶς
ἐκ τῶν τοῦ διαβόλου δεσμῶν. 5.5 οὐκοῦν τὰ γενόμενα παράδοξα σημεῖα παρὰ τοῦ
Ἰησοῦ· κἂν γὰρ αἰσθητῶς κατὰ τὴν ἱστορίαν ἐγένετο, ἀλλὰ νοητῶν χάριν ἐγένετο.
ἐτελεῖτο γοῦν τὰ τῆς ἱστορίας καὶ πράγματα ἦν τὰ σημαινόμενα. 5.6 εἰ μὴ γὰρ
πράγματα θεῖα ἦν τὰ σημαινόμενα, οὐκ ἂν οὕτως ἔμελλε τοῖς προφήταις προκηρῦξαι
αὐτοῦ τὴν ἐπιδημίαν. 5.7 Ἠσαΐας γὰρ βοᾷ λέγων· «ἰδοὺ ὁ βασιλεύς σου ἔρχεταί σοι
πραῢς καὶ ἐπιβεβηκὼς ἐπὶ ὄνον καὶ πῶλον υἱὸν ὑποζυγίου.» 5.8 ἐπὶ τούτοις τοῖς
γενομένοις παραδόξοις σημείοις παρὰ τοῦ Ἰησοῦ οὐκ ἔστιν οὔτε προσθῆναι οὔτε
ἀφελεῖν. πάντα γὰρ τελείως καὶ ἀνελλιπῶς ἐποίησεν. ἦν γὰρ «δυνατὸς ἐν ἔργοις καὶ
λόγοις» αὐτοῦ. 5.9 ἀμέλει τούτων μηνύων καὶ ὁ ψαλμῳδὸς ἔλεγε· «περίζωσαι τὴν
ῥομφαίαν σου ἐπὶ τὸν μηρόν σου, δυνατέ, τῇ ὡραιότητί σου καὶ τῷ κάλλει σου καὶ
ἔντεινον καὶ κατευοδοῦ καὶ βασίλευε.» τῇ γοῦν δυνάμει αὐτοῦ τῇ μεγάλῃ πᾶσαν
ἠλευθέρωσε τὴν ἀνθρωπότητα. 6.1 Ταῦτα δὲ ἐποίησε τὰ σημεῖα παρὼν αὐτὸς ὁ
κύριος μεθ' ἡμῶν, ἵνα φοβηθῶμεν ἀπὸ προσώπου αὐτοῦ. 6.2 ὅτι δὲ τὰ αἰσθητὰ
 
 
 
 

3

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

σύμβολα νοητῶν ἐστιν, ἄκουε. ἐπεδήμησε γὰρ ὁ σωτήρ, ἵνα τὴν τοῦ ἀνθρώπου
ψυχὴν προηγουμένως εὐεργετήσῃ. 6.3 ἀλλ' ἐπειδὴ τοῦτο τοῖς πολλοῖς ἄγνωστον ἦν
διὰ τῶν ὁρωμένων ἰάσεων τὴν κεκρυμμένην τῆς ψυχῆς σωτηρίαν ἐδείκνυεν. 6.4
ἀμέλει τῷ παραλυτικῷ ἄφεσιν ἁμαρτιῶν δωρησάμενος ἠπιστεῖτο. ἔλεγον γὰρ οἱ
Ἰουδαῖοι· «τίς δύναται ἁμαρτίας ἀφιέναι εἰ μὴ μόνος ὁ θεός;» 6.5 εἶτα, ἐπειδὴ τὸ
ἀφανὲς καὶ προηγούμενον ποιήσας ἐλάνθανε, κατὰ δεύτερον λόγον καὶ τὸ σῶμα
ἰάσατο. εἶπε γὰρ αὐτῷ· «ἔγειρε, ἆρον τὸν κράβατόν σου καὶ περιπάτει.» 6.6 ὁρᾷς πῶς
ταχέως οὗτος λέλυται ἀπὸ τῶν δεσμῶν καὶ οὐ μόνον διὰ τῆς ἔξωθεν ἰάσεως λέλυται,
ἀλλὰ καὶ τῶν τοσούτων ἁμαρτημάτων ἐλευθεροῦται. 6.7 ἔτι μὴν καὶ κήρυκα τῆς
θεραπείας αὐτὸν ἀπέδειξε καὶ ὅλως τὰ γενόμενα αἰσθητῶς νοητῶν χάριν ἐγένοντο.
6.8 διά τοι τοῦτο ἀποστέλλονται καὶ οἱ μαθηταὶ εἰς τὴν κατέναντι κώμην καὶ λύουσι
τὴν ὄνον καὶ τὸν πῶλον κατὰ τὰ κελευσθέντα αὐτοῖς ὑπὸ τοῦ διδασκάλου. 6.9 τοῦτο
δὲ ὅλως γέγονεν, ἵνα πληρωθῇ καὶ ἡ τοῦ Ἰακὼβ προφητεία. εὐλογῶν γὰρ τοὺς
δώδεκα πατριάρχας φθάσας εἰς τὸν Ἰούδαν προφητεύει καὶ θεῖα ῥήματα φθέγγεται.
6.10 ἄκουε γὰρ αὐτοῦ λέγοντος· «δεσμεύων» γὰρ «πρὸς ἄμπελον τὸν πῶλον αὐτοῦ
καὶ τῇ ἕλικι τὸν πῶλον τῆς ὄνου αὐτοῦ.» 6.11 καὶ ὅλως τὰ λεχθέντα ἅπαντα εἰς τὸν
Ἰούδαν εὑρήσεις εἰς τὴν παρουσίαν τοῦ σωτῆρος προφητευόμενα. δύναται γὰρ ὁ
φιλομαθὴς κατ' ὀλίγον ἀναγινώσκων νοῆσαι τὰ περὶ αὐτοῦ εἰρημένα. 6.12 ἵνα δὲ μὴ
πᾶσαν τὴν ἱστορίαν διηγησώμεθα, ἀκούσωμεν τοῦ ἀποστόλου λέγοντος· «πρόδηλον
γὰρ ὅτι ἐξ Ἰούδα ἀνατέταλκεν ὁ κύριος ἡμῶν.» 7.1 ∆ιὰ τοῦτο γοῦν ἐγένοντο τὰ
γενόμενα, ἵνα τὸν δημιουργὸν διὰ τῆς ἑαυτοῦ λειτουργίας φανερὸν ποιήσωσι. 7.2 τίς
γὰρ θεασάμενος ναῦν ἄριστα εὐθυνομένην οὐκ ἔννοιαν δέχεται κυβερνήτου; ἅρμα
δὲ καλῶς διαβαῖνον καὶ τεχνικαῖς ἡνίαις φερόμενον οὐκ ἐννοεῖ τὸν ἡνιοχοῦντα; εἰ
οὖν ἐπὶ τῶν ἐν χερσὶ τοιαῦτα, τί ἂν εἴποιμεν ἐπὶ τῶν τοῦ σωτῆρος παραδόξων; 7.3
ὁρῶμεν γὰρ τῶν ποιημάτων αὐτοῦ τὰ φοβερὰ καὶ παράδοξα· οἷον θαλάσσης τὰς
τρικυμίας καὶ τῶν ἐν αὐτῇ προσαρασσόντων ἀνέμων τὰς κινήσεις, ἡλίου τε τὸν
κύκλον καὶ σελήνης τὸν δρόμον καὶ τῶν ἀστέρων τὴν σύνθεσιν. 7.4 τίς δὲ οὐκ
ἐφοβήθη, ὅτε «τὸ καταπέτασμα τοῦ ναοῦ ἐσχίσθη ἀπὸ ἄνωθεν ἕως κάτω,» ὅτε ὁ
ἥλιος οὐ φέρων τὸ τολμηθὲν φόβῳ τὸ φῶς ἔστειλεν, ἢ ὅτε «αἱ πέτραι ἐσχίσθησαν
καὶ τὰ μνημεῖα ἠνεῴχθησαν» καὶ οἱ ἐναποκείμενοι ἀνέστησαν καὶ οἱ ἐχθροὶ
ἐφοβήθησαν ἀπὸ προσώπου αὐτοῦ; διὸ καὶ ἄκοντες ὡμολόγουν ὅτι «ἀληθῶς θεοῦ
υἱὸς ἦν οὗτος.» 7.5 καὶ ὅλως «τὰ πλείονα τῶν ἔργων αὐτοῦ ἐν ἀποκρύφοις ἐστί.»
«χίλιαι» γὰρ «χιλιάδες παρειστήκεισαν αὐτῷ αἰνοῦντες καὶ μύριαι μυριάδες
λειτουργοῦσιν αὐτῷ.» 7.6 ἐπιγνῶμεν οὖν καὶ ἡμεῖς τὸν εὐεργέτην. δοξάσωμεν τὸν
πατέρα σὺν υἱῷ καὶ ἁγίῳ πνεύματι· μίαν θεότητα ὁμολογήσωμεν. 7.7 τοῦ
ἁμαρτάνειν παυσώμεθα. τῶν πτωχῶν μνημονεύσωμεν. ἡ φιλαδελφία μενέτω. τῆς
φιλοξενίας μὴ ἐπιλανθάνεσθε. 7.8 οὕτω γὰρ διάγοντες βασιλείαν οὐρανῶν
κληρονομήσωμεν ἐν Χριστῷ Ἰησοῦ τῷ κυρίῳ ἡμῶν, ᾧ ἡ δόξα καὶ τὸ κράτος εἰς τοὺς
αἰῶνας τῶν αἰώνων. ἀμήν.

 
 
 
 

4

Ερευνητικό έργο: ∆ΡΟΜΟΙ ΤΗΣ ΠΙΣΤΗΣ – ΨΗΦΙΑΚΗ ΠΑΤΡΟΛΟΓΙΑ.
Εργαστήριο ∆ιαχείρισης Πολιτισµικής Κληρονοµιάς, www.aegean.gr/culturaltec/chmlab.
Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
Πανεπιστήµιο Αιγαίου, Τµήµα Πολιτισµικής Τεχνολογίας και Επικοινωνίας, © 2006.
Επιτρέπεται η ελεύθερη χρήση του υλικού µε αναφορά στην πηγή προέλευσής του.

