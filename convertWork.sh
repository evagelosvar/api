#!/bin/bash
#∆ Δ
workFile="$1"
echo "$workFile"
author_name=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\2@g'|sed -r 's@([^_]*)_(.*)@\1@g'`
work_title=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\3@g'`
work_url="$workFile"

#author_id=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\2@g'|sha1sum|sed -r 's@ \*-@@g'`
author_id=`echo "$author_name"  |sha1sum|sed -r 's@([^ ]*)[ ].*@\1@g'`
work_id=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\3@g'|sha1sum|sed -r 's@([^ ]*)[ ].*@\1@g'`
#sed -r 's@([^ ]*)[ ].*@\1@g'

id=$author_id-$work_id

#################################
split  --additional-suffix=".frg" -a 5 -d -l 5 "$workFile" fragments/fragment-$id- #fragmnets
#################################

find fragments -name "*$id-*.frg"| while read  fragment
do
######################
#START WORD JSON
#######################

jsonWord=',
{
"fragment_url"	:"'"$fragment"'",
"author_name"	:"'"$author_name"'",
"work_title"	:"'"$work_title"'",
"work_url"		:"'"$work_url"'"
}'
#pass filter
#|tr "<>.\'\"(){}[]+?\-_/\=ϟ+&$®§" ' ' 
#########################################################
files=`cat $fragment |\
sed -r 's@(\"|\*|'"'"'|<|>|\?|,|\.|;|#|Â©|%|:|%)@ @g'|tr ".(){}[]+?\-_" ' ' | sed 's@·@@g' |\
xargs -n 1 echo words/|sed -r 's@\/ @\/@g' | xargs echo`
#########################################################
echo "$files"
echo "$jsonWord" | tee -a `echo "$files"` >/dev/null



######################
#MAKE FRAGMENT JSON
######################
fragment_content=`cat $fragment|sed  -r ':a;N;$!ba;s@\n@ @g'|sed -r 's@\"@ @g'` #pass filter

jsonFragment='[
{
"fragment_id"		:"'"$fragment"'",
"fragment_content"	:"'"$fragment_content"'",
"author_name"		:"'"$author_name"'",
"work_title"		:"'"$work_title"'",
"work_url"			:"'"$work_url"'"
}
]'

echo "$jsonFragment" > $fragment 
#########################
done
