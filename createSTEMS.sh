#!/bin/bash
list=list
words=words
find "$words" -type f|xargs -n 1 -I % basename %|\
while read f;
do
TL=`echo "$f"|sed -r 's@(..).*@\1@g'`
echo "$f" >>"$list""/""$TL"
done


find "$list" -type f |\
while read f;
do
cat "$f"|sed -r 's@^.*$@\"&\"\n,@g'|sed -r '1{s@^.*$@[&@g}'| sed -r '${s@,@\]@g}' > "$f".tmp
mv "$f".tmp "$f"
done 
