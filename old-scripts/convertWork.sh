#!/bin/bash

workFile="$1"
echo "$workFile"
author_name=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\2@g'|sed -r 's@([^_]*)_(.*)@\1@g'`
work_title=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\3@g'`
work_url="$workFile"

#author_id=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\2@g'|sha1sum|sed -r 's@ \*-@@g'`
author_id=`echo "$author_name"  |sha1sum|sed -r 's@ \*-@@g'`
work_id=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\3@g'|sha1sum|sed -r 's@ \*-@@g'`

id=$author_id-$work_id

#################################
split  --additional-suffix=".frg" -a 5 -d -l 5 "$workFile" fragments/fragment-$id- #fragmnets
#################################

find fragments -name "*$id-*.frg"| while read  fragment
do
######################
#START WORD JSON
#######################

jsonWord=',
{
"fragment_url"	:"'"$fragment"'",
"author_name"	:"'"$author_name"'",
"work_title"	:"'"$work_title"'",
"work_url"		:"'"$work_url"'"
}'
#pass filter
#########################################################
files=`cat $fragment |\
sed -r 's@'"'"'@@g' |\
xargs -n 1 echo words/|sed -r 's@\/ @\/@g' | xargs echo`
#########################################################
echo "$jsonWord" | tee -a `echo "$files"` >/dev/null


######################
#MAKE FRAGMENT JSON
######################
fragment_content=`cat $fragment` #pass filter

jsonFragment='[
{
"fragment_id"		:"'"$fragment"'",
"fragment_content"	:"'"$fragment_content"'",
"author_name"		:"'"$author_name"'",
"work_title"		:"'"$work_title"'",
"work_url"			:"'"$work_url"'"
}
]'

echo "$jsonFragment" > $fragment 
#########################
done
