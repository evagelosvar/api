#!/bin/bash

#aws --endpoint-url http://s3.nl-ams.scw.cloud s3 ls
#exec &> logfile.txt
######################
#≡14__038≡>   Ηψατο
#140 authisapo 489 1524
#######################
#run after main.sh ..........pcloud.sh, intemplate.sh,indirectory.sh
#split -l 3 -d --additional-suffix=.html results.html results
currentDir=`dirname $0`
echo $currentDir
wordsFolder="/root/artifacts/words"
subfolder='Basilius Caesariensis_PG 29-32'




#subfolder='test'

subfolder=
subfolder=[A-I]*


#find "$currentDir""/""pmg""/"$subfolder

subfolder='Agathias Scholasticus Myrinaeus,Constantinopolitanus_PG 88'


subfolder=[A-N]*
subfolder=[A-T]*

subfolder='test'
writeGITLAB() {
repo="$1"

cd "$currentDir"
rm -rf  "$repo"

#mkdir "$currentDir""/""$repo"
#git clone https://evagelosvar-at-351320785362:5Zu2nN1+67dejftHod9uUOpkqP6XcIcg2G0n/dxKbDI=@git-codecommit.us-west-2.amazonaws.com/v1/repos/evagelosvar
git clone https://$USER:$PASS@gitlab.com/PHD-Patrologia/"$repo".git
#git clone https://$USER:$PASS@gitlab.com/PHD-Patrologia/data.git
#git clone https://$USER:$PASS@gitlab.com/PHD-Patrologia/PHD.git
#https://$USER:$PASS@gitlab.com/patrologia/search.git
#git push https://c354pyesv3xzrt7ic3mblyrugre4ix576ameyr4nmyzrrmuq6rdq@patrologia.visualstudio.com/words/_git/words

#https://evagelosvar:marina12gmail@@evagelosvar.visualstudio.com/pcloud3/_git/pcloud3
#ls
rm -rf  "$repo""/""words"

mkdir "$repo""/""words"


cp -R  "words" "$repo"



#cp  -r /builds/PHD-Patrologia/PHD/nlp/words/ /builds/PHD-Patrologia/PHD/nlp/data/
#echo "========="
##ls /builds/PHD-Patrologia/PHD/nlp/data/words
#exit
echo "========="
cd "$repo"
#ls -a
#ls words

#echo "========="
echo "test2" > test
git config --global user.name "evagelosvar"
git config --global user.email "evagelosvar@gmail.com"
git add .
git commit -m "pslo"
USER=evagelosvar
PASS=marina12
git push https://$USER:$PASS@gitlab.com/PHD-Patrologia/"$repo".git HEAD:master
exit
cd ..
#rm -rf  "$repo"
}



format() {
x="$1"
echo "<b><font size=6>""$x""<\/font><\/b>"

}
createdirectories() {
#htmlFolder="html"

#mkdir "htmlFolder"
files=`find "$wordsFolder" -type f -name '*'`
#echo "$files"
for folderfile in `echo "$files"`
do


#echo "$currentDir""/""$wordsFolder""/"`basename "$folderfile"`


mv "$folderfile" "$folderfile"".html" 
mkdir "$folderfile"
mv "$folderfile"".html" "$folderfile""/""index.html" 
#echo    "$folderfile"".html"
#exit
 

done
	
	
}






insertTemplate() {

files=`find "$wordsFolder" -type f -name '*'`
#echo "$files"
for file in `echo "$files"`
do
#echo "$file"
sed -i '1i {{}}' "$file"
sed -i '$a \<script src="../../js/js.js"\>\<\/script\>' "$file"
done


#cp -r js word/js



}






cutLength() {
IFS='
'
if [ ! -t 0 ] 
    then
        INPUT=`cat`
    else
        INPUT=""
    fi
tmplist=''
for word in `echo "$INPUT"`
do
#echo "-------"
#echo $word
length=`echo -n $word | wc -c`
#echo $length

if [ "$length" -lt 8 ]
then


continue
else

tmplist=$word'
'$tmplist

fi
done
echo "$tmplist"




}

paragraphTokeniser() {
	
	if [ ! -t 0 ] 
    then
        INPUT=$(cat)
    else
        INPUT=""
    fi
    
filter="Ερευνητικό έργο:|Εργαστήριο ∆ιαχείρισης|Χρηµατοδότηση|Πανεπιστήµιο Αιγαίου|Επιτρέπεται"

# removes filter text
   INPUT=`echo "$INPUT" | sed -r 's@^('"$filter"').*$@@g'`
    
# removes \n, \r and cut to points (\.|·)
	echo "$INPUT"  | sed -r ':a;N;$!ba;s/\n/ /g'| sed -r 's/\r//g' | sed -r 's@(\.|·|˙|;)@\1\n@g'

	
}


xxx_wordTokeniser() {
	if [ ! -t 0 ] 
    then
        INPUT=$(cat)
    else
        INPUT=""
    fi
	
	
filter=\''|'\"'|'\\['|'\\]'|'\\*'|'\<'|'\>\
'|'\\{'|'\\}'|'«'|'»'|'\\\('|'\\\)'|'[0-9]+\
'|'̓'|'–'|'©'|'%'|'\\\\
#more “ ” ‘ ˉχˉυ ʹ #ϛφπθʹ - Ϡπηʹ Ακουε :
echo "$INPUT"  | tr ' ' '\n' | sort | uniq|\
sed -r 's@[,|.|·|;]@@g' |\
sed -r 's@('"$filter"')@@g'|\
sed -r 's@[^[:print:]]@@g' |\
sed -r '/^[ \t]*$/d'


#| sed -r 's@(^.*)$@\1=>\[Go\]\('"$relFilePath"'\)@g' >> $currentDir"/docs/"$wordsFolder"/"$listFile	
}

wordTokeniserOnly() {
	if [ ! -t 0 ] 
    then
        INPUT=$(cat)
    else
        INPUT=""
    fi
	
	


filter=\''|'\"'|'\\['|'\\]'|'\\*'|'\<'|'\>\
'|'\\{'|'\\}'|'«'|'»'|'\\\('|'\\\)'|'[0-9]+\
'|'̓'|'–'|'©'|'%'|'\\\\'|'#'|'“'|'”'|'-'|'‘'|':


#more “ ” ‘ ˉχˉυ ʹ #ϛφπθʹ - Ϡπηʹ Ακουε :
echo "$INPUT"  | tr ' ' '\n' |\
sed -r 's@[,|\.|·|;|˙|;]@@g' |\
sed -r 's@('"$filter"')@@g'|\
sed -r 's@[^[:print:]]@@g' |\
sed -r '/^[ \t]*$/d'


#| sed -r 's@(^.*)$@\1=>\[Go\]\('"$relFilePath"'\)@g' >> $currentDir"/docs/"$wordsFolder"/"$listFile	
}

#Χρηµατοδότηση: ΚΠ Interreg ΙΙΙΑ (ETΠΑ 75%, Εθν. πόροι 25%).
count=0
main() {
 
 
cd "$currentDir"
 
#exit

 
rm -r "$wordsFolder"
mkdir -p "$wordsFolder"

IFS='
'
files=`find "pmg""/"$subfolder -type f -name '*.txt'`

for file in `echo "$files"`
do

echo "$file"

author=`basename $file`
author=$file
path="../../""$author"
count=0
for paragraph in `cat "$file"|paragraphTokeniser`
#|\
#sed  '$a 99999' |sed =|sed  '1i 00000'  | sed 'N;s@\n@\t ['"path#"'@'`
#thelei kaliteri kataskeui
do
count=`expr $count + 1`

#listWords=`echo "$paragraph"|wordTokeniserOnly|cutLength`

listWords=`echo "$paragraph"|wordTokeniserOnly`

for word in `echo "$listWords"`
do
:
paragraph=`echo $paragraph|sed 's@path@'"$path"'@'`
echo $paragraph"[$path#$count]">> "$wordsFolder""/"$word

done



done

done
#numbering methods
#https://linuxcommando.blogspot.com/2008/06/how-to-number-each-line-in-text-file-on.html
du -sh ./

#sh intemplate.sh

find $wordsFolder |sort > index.html

sh indirectory.sh

mv index.html $wordsFolder"/"index.html


#tar -zcvf "$currentDir""/""words.tar.gz" "$currentDir""/""words"
tar -zcf "/root/artifacts/words.tar.gz" $wordsFolder
#cp "words.tar.gz" /root/pCloudDrive/p/


}
main

exit

mkdir words
mkdir tmp
mkdir ttt
echo "ooo">words/fff
echo "ooo">words/fffrr

cp -R words ttt
ls ttt/words
echo "============1"
cp  words/* tmp

ls tmp
echo "============2"
mv words tmp/words
ls tmp/words
echo "============3"
ls tmp
exit








