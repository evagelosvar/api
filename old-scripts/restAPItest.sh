#!/bin/bash

#s3cmd put index.html s3://test123123/ --recursive -m text/html --acl-public

cd test
corpusFolder=PG_Migne_FULL
echo '[' >tree
numofAuthors=`find "$corpusFolder""/" -mindepth 1 -type d |sort|wc -l`
count_author=0

find "$corpusFolder""/" -mindepth 1 -type d |sort|\
while read  author_folder
do

count_author=`expr $count_author + 1`
echo $author_folder
author_name=`echo "$author_folder"|sed -r 's@(^.*)/(.*)@\2@g'|sed -r 's@([^_]*)_(.*)@\1@g'`
author_id=`echo "$author_name" |sha1sum|sed -r 's@ \*-@@g'`
volumes=`echo "$author_folder"|sed -r 's@(^.*)/(.*)@\2@g'|sed -r 's@[^_]*_PG\s(.*)@\1@g'`

#echo "$volumes"|cut -d'-' -f1

#exit

jsonTree1='{
"author_id"		:"'"$author_id"'",
"author_name"	:"'"$author_name"'",
"volumes"		:['"$volumes"'],
"works":
['

echo "$jsonTree1" >>tree
count_work=0
numofWorks=`find "$author_folder""/" -type f -name '*.txt'|sort|wc -l`

find "$author_folder""/" -type f -name '*.txt'|sort|\
while read  workFile
do
count_work=`expr $count_work + 1`
echo $workFile
work_title=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\3@g'`
work_id=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\3@g'|sha1sum|sed -r 's@ \*-@@g'`

jsonTree2B='{
"work_id"		:"'"$work_id"'",
"work_title"	:"'"$work_title"'"
},'

if [ $count_work -lt $numofWorks ]
then
jsonTree2B='{"work_id"		:"'"$work_id"'","work_title"	:"'"$work_title"'"},'
else
jsonTree2B='{"work_id"		:"'"$work_id"'","work_title"	:"'"$work_title"'"}'
fi

echo "$jsonTree2B" >>tree
done


if [ $count_author -lt $numofAuthors ]
then
jsonTree2E=']
},'

:
else
:
jsonTree2E=']
}
]'
fi

echo "$jsonTree2E" >>tree

done



#####################
#MAIN ROUTINE
#####################
words=words
fragments=fragments

rm -R "$words"
rm -R "$fragments"
mkdir -p "$words"
mkdir -p "$fragments"
find "$corpusFolder""/" -type f -name '*.txt'|sort|\
while read  workFile
do
echo $workFile
author_name=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\2@g'|sed -r 's@([^_]*)_(.*)@\1@g'`
work_title=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\3@g'`
work_url="$workFile"



#author_id=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\2@g'|sha1sum|sed -r 's@ \*-@@g'`
author_id=`echo "$author_name"  |sha1sum|sed -r 's@ \*-@@g'`
work_id=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\3@g'|sha1sum|sed -r 's@ \*-@@g'`

id=$author_id-$work_id

#################################
split  --additional-suffix=".frg" -a 5 -d -l 5 "$workFile" fragments/fragment-$id- #fragmnets
#################################

find fragments -name "*$id-*.frg"| while read  fragment
do
######################
#START WORD JSON
#######################

jsonWord=',
{
"fragment_url"	:"'"$fragment"'",
"author_name"	:"'"$author_name"'",
"work_title"	:"'"$work_title"'",
"work_url"		:"'"$work_url"'"
}'


files=`cat $fragment |\
sed -r 's@'"'"'@@g' |\
xargs -n 1 echo words/|sed -r 's@\/ @\/@g' | xargs echo`

echo "$jsonWord" | tee -a `echo "$files"` >x





######################
#MAKE FRAGMENT JSON
######################
fragment_content=`cat $fragment`

jsonFragment='[
{
"fragment_id"		:"'"$fragment"'",
"fragment_content"	:"'"$fragment_content"'",
"author_name"		:"'"$author_name"'",
"work_title"		:"'"$work_title"'",
"work_url"			:"'"$work_url"'"
}
]'


echo "$jsonFragment" > $fragment 

done


done


find  "words" -type f  -exec sed -i '1{s@,@[@g}' {} +
find  "words" -type f  -exec sed -i '${s@\}@\}\n\]@g}' {} +

exit






























#s3cmd put index.html s3://test123123/ --recursive -m text/html --acl-public

cd test
#echo test|xargs find
corpusFolder=PG_Migne_FULL

#echo -n "ddd/o2" | sha1sum


find "$corpusFolder""/" -mindepth 1 -type d|\
while read  authorFolder
do
(( countAUTHOR += 1 ))
#let countAUTHOR=$countAUTHOR + 1
echo $authorFolder
echo $countAUTHOR
find "$authorFolder""/" -type f -name '*.txt'|\
#countWORK = 0
while read  workFile
do
(( countWORK += 1 ))


##########
echo $workFile
echo "->"$countWORK

id=$countAUTHOR-$countWORK
echo $id
#echo $workFile|xargs -n 1 -I % 
split  --additional-suffix=".frg" -a 5 -d -l 5 "$workFile" fragments/fragment-$id- #fragmnets


#find fragments -name "*$countAUTHOR-$countWORK-*.frg"
find fragments -name "*$countAUTHOR-$countWORK-*.frg"| while read  fragment
do

echo $fragment

echo "=============="


json='{
"fragment_url"	:'"$fragment"',
"author_name"	:'"$authorFolder"',
"work_title"	:'"$workFile"',
}
,'
#echo "$json"
files=`cat $fragment |\
sed -r 's@'"'"'@@g' |\
xargs -n 1 echo words/|sed -r 's@\/ @\/@g' | xargs echo`

echo "$json" | tee -a `echo "$files"`

echo "=============="
#|sed -r 's@"@@'
#printf "%q\n" 
:
done



#############
done
done

exit













find fragments -name '*.frg'| while read  file
do

echo "->"$file
#cat "$file" | tee -a `cat $file`

cat $file|sed -r 's@"@@'
#|\
xargs -n 1 echo words/|\
sed -r 's@\/ @\/@g' |xargs echo
#cat "$file"


#cat "$file" |\
tee -a `cat $file|\
sed -r 's@"|"@@'|\
xargs -n 1 echo words/|\
sed -r 's@\/ @\/@g' |\
xargs echo`

#cat "$file" | tee -a `cat $file|sed -r 's@"@@'`


done



sentence="this is  the sentence    'you' want to split"
words=( $sentence )
printf "%s\n" "${words[@]}"


exit
























find . -name '*.txt'|xargs -n 1 echo $count


find . -name '*.txt'|xargs -n 1 -I % split --additional-suffix=".frg" -a 5 -d -l 2  #fragmnets



 


 
#while read NAME; do echo $NAME ; done < <(find html -type f -name '*.html')

#while read NAME; do echo $NAME ; done < <(find . -name '*.frg')



exit


files=`find . -name '*.frg'`
for file in `echo "$files"`
do
cat "$file" | tee -a `cat $file`
done
#files=`find . -name '*.frg'|xargs -n 1 cat`



exit





files=`find . -name '*.frg'|xargs  cat`
#|sed -r 's@x@2@g'`
		
input=`find . -name '*.frg'|xargs cat`

echo "$input" | tee -a `echo $files`
echo $files


exit
sentence="this is the "sentence"   'you' want to split"
words=( $sentence )

len="${#words[@]}"
echo "words counted: $len"

printf "%s\n" "${words[@]}" ## print array
files=`printf "%s\n" "${words[@]}"|xargs echo`
echo xxxteast | tee -a `echo "$files"`


#split -l 2 spli





exit






find /mnt/disk2/artifacts/base/fragments | parallel  s3cmd put {/.} s3://test123123/fragments/{/.} -m text/html --acl-public
exit
s3cmd sync /mnt/disk2/artifacts/base/fragments/ s3://test123123/fragments/ -m text/html --acl-public

s3cmd sync /mnt/disk2/artifacts/base/authors/words/ s3://test123123/authors/words/ -m text/html --acl-public
exit


find  /mnt/disk2/artifacts/base/fragments/  -exec sed -i '/^$/d' {} +


find  /mnt/disk2/artifacts/base/fragments/ -exec sed -i '${s@,@]@g}' {} +
exit



find  /mnt/disk2/artifacts/base/authors/words/  -exec sed -i '/^$/d' {} +


find  /mnt/disk2/artifacts/base/authors/words/ -exec sed -i '${s@,@]@g}' {} +


#aws --endpoint-url http://s3.nl-ams.scw.cloud s3 ls
#exec &> logfile.txt
######################
#≡14__038≡>   Ηψατο
#140 authisapo 489 1524
#######################
#run after main.sh ..........pcloud.sh, intemplate.sh,indirectory.sh
#split -l 3 -d --additional-suffix=.html results.html results
currentDir=`dirname $0`
#echo $currentDir
#echo $currentDir










rootFolder="C:/Users/ainok/Documents/base"
rootFolder="/root/artifacts/base"
rootFolder="/mnt/disk2/artifacts/base"
ls $rootFolder

#sh "C:/Users/ainok/Documents/MEGAsync/phd/wordsPHD/restAPI.sh"

subfolder='Basilius Caesariensis_PG 29-32'

subfolder=
subfolder=[A-I]*

subfolder='Agathias Scholasticus Myrinaeus,Constantinopolitanus_PG 88'
subfolder=[A-N]*
subfolder=[A-T]*

subfolder='test'

createsearchlist () {
#read string
#check=${#string}
#echo $check
#if [ $check -ge 5 ]; then echo "error" ; exit
#else echo "done"
#fi
#str="οἰχομένοις"
#var="${str:0:2}"
#echo $var

#exit
	
	
	
	
	list=list
	rm -r "$rootFolder/$list"
	mkdir -p $rootFolder/$list
	
	echo $rootFolder/$apiFolderAuthors/$apiFolderWords
	files=`find "$rootFolder/$apiFolderAuthors/$apiFolderWords" -type f|sort`
	
	for file in `echo "$files"`
	do
	#echo $file
	word=`basename $file`
	cuta=`echo "$word" |sed -r 's@(^..).*@\1@g'`
	#cuta="${word:0:2}"
	#echo $word
	#echo $cuta
	if [ ! -f $rootFolder/$list/$cuta ]
	then
	printf [\\n >$rootFolder/$list/$cuta
	fi
	
	
	printf '"'"$word"'"'\\n,\\n >> $rootFolder/$list/$cuta
	
	
	done
	
	files=`find "$rootFolder/$list" -type f`
	for file in `echo "$files"`
	do
	#echo $file
	sed -i '/^$/d' $file
    sed -r -i -e '${s@,@]@g}' $file
	done
	
	
	

	
	
	
	
	
}






createFinalJson () {
	#apiFolderAuthors=authors
	#find rootFolder
	IFS='
	'
	#[ ! -f /path/to/file ] && echo "File not found!"
	#if [ ! -f "$rootFolder"/"ddd" ]
	#then
	#echo "ppp"
	#exit
	
	#fi
	
	#ls $rootFolder
	echo $rootFolder
	files=`find "$rootFolder" -type f`
	for file in `echo "$files"`
	do
	
	
	
	
	#sed -i '1s/^/[\n/' $file
	echo $file
	#sed -i -e '$a]' $file
	
    sed -i '/^$/d' $file
    sed -r -i -e '${s@,@]@g}' $file
	done
}




createfragmentIDandTokenising () {

#echo "" > "$rootFolder/"$apiFolderFragments

	
	file="$3"
	authorID="$1"
	textID="$2"
#echo $3







author_id_text_id_dir="$rootFolder/$apiFolderAuthors/$authorID/$apiFolderTexts/$textID/$apiFolderWords/"
author_id_dir="$rootFolder/$apiFolderAuthors/$authorID/$apiFolderWords/"
authors_dir="$rootFolder/$apiFolderAuthors/$apiFolderWords/"

mkdir -p $author_id_text_id_dir
mkdir -p $author_id_dir
mkdir -p $authors_dir




echo  '================='
echo  "$file"
echo  '================='


fragmentID=0
#cat $file|wc -l

for fragment in `cat "$file"|paragraphTokeniser`
do

if [ $fragmentID -gt 111111111 ]
then
continue

else
###########
# create only fragment
###########
fragmentID=`expr $fragmentID + 1`

echo '
{
"fragmentid":"'$authorID-$textID-$fragmentID'",
"fragmentcontent": "'$fragment'"
}'>> "$rootFolder/$apiFolderFragments/""$authorID-$textID-$fragmentID"


##############
###########
# process and tokenise the fragment
###########
listWords=`echo "$fragment"|wordTokeniserOnly`


#?##/v1/patrologia/authors/id_author/texts/id_text/words/id_word/urls 


#echo  '================='
#echo $authorID-$textID-$fragmentID
#echo $listWords
#echo $word
#echo  "$file"

for word in `echo "$listWords"`
do
authors_file="$rootFolder/$apiFolderAuthors/$apiFolderWords/"$word
author_id_file="$rootFolder/$apiFolderAuthors/$authorID/$apiFolderWords/"$word
author_id_text_id_file="$rootFolder/$apiFolderAuthors/$authorID/$apiFolderTexts/$textID/$apiFolderWords/"$word

#echo '{
#'"'"url"'"' : '"'"/$apiFolderFragments/$authorID-$textID-$fragmentID"'",'
#'"'"$authorID-$textID-$fragmentID"'"' : '"'"$fragment"'",'
 #},
 #' >> $author_id_text_id_file

#'"'"url"'"' : '"'"/$apiFolderFragments/$authorID-$textID-$fragmentID"'",'
#'"'"$authorID-$textID-$fragmentID"'"' : '"'"$fragment"'",'
#},
#' >> $author_id_file



if [ ! -f $authors_file ]
then
echo '
[
' > $authors_file
fi
if [ ! -f $author_id_file ]
then
echo '
[
' > $author_id_file
fi
if [ ! -f $author_id_text_id_file ]
then
echo '
[
' > $author_id_text_id_file
fi








#,
#"'$authorID-$textID-$fragmentID'" : "'$fragment'"


echo '{
"url" : "'/$apiFolderFragments/$authorID-$textID-$fragmentID'"
}
,
' | tee -a $authors_file $author_id_file $author_id_text_id_file > /dev/null 2>&1

#echo "hello" | tee -a file1 file2

#paragraph=`echo $paragraph|sed 's@path@'"$path"'@'`
#echo '{'>> "$rootFolder""/$apiFolderWords/"$word
#	echo '"'"$authorID-$textID-$fragmentID"'"' : '"'"$fragment"'",' >> "$rootFolder""/$apiFolderWords/"$word
#echo '"'"url"'"' : '"'"/fragments/$authorID-$textID-$fragmentID"'",' >> "$rootFolder""/$apiFolderWords/"$word

#echo '},'>> "$rootFolder""/$apiFolderWords/"$word
done




fi

done
	
	
}

paragraphTokeniser() {
	
	if [ ! -t 0 ] 
    then
        INPUT=$(cat)
    else
        INPUT=""
    fi
    
filter="Ερευνητικό έργο:|Εργαστήριο ∆ιαχείρισης|Χρηµατοδότηση|Πανεπιστήµιο Αιγαίου|Επιτρέπεται"

# removes filter text
   INPUT=`echo "$INPUT" | sed -r 's@^('"$filter"').*$@@g'`
    
# removes \n, \r and cut to points (\.|·)
	echo "$INPUT"  | sed -r ':a;N;$!ba;s/\n/ /g'| sed -r 's/\r//g' | sed -r 's@(\.|·|˙|;)@\1\n@g'

	
}

wordTokeniserOnly() {
	if [ ! -t 0 ] 
    then
        INPUT=$(cat)
    else
        INPUT=""
    fi
	
	


filter=\''|'\"'|'\\['|'\\]'|'\\*'|'\<'|'\>\
'|'\\{'|'\\}'|'«'|'»'|'\\\('|'\\\)'|'[0-9]+\
'|'̓'|'–'|'©'|'%'|'\\\\'|'#'|'“'|'”'|'-'|'‘'|':


#more “ ” ‘ ˉχˉυ ʹ #ϛφπθʹ - Ϡπηʹ Ακουε :
echo "$INPUT"  | tr ' ' '\n' |\
sed -r 's@[,|\.|·|;|˙|;]@@g' |\
sed -r 's@('"$filter"')@@g'|\
sed -r 's@[^[:print:]]@@g' |\
sed -r 's@@@g'|\
sed -r '/^[ \t]*$/d'


#| sed -r 's@(^.*)$@\1=>\[Go\]\('"$relFilePath"'\)@g' >> $currentDir"/docs/"$rootFolder"/"$listFile	
}

createauthors() {
IFS='
'

echo "" > "$rootFolder/"$apiFolderAuthors
echo "{" > "$rootFolder/"$apiFolderAuthors
 
authors=`find "pmg""/" -mindepth 1 -type d `
authorID=0
for author in `echo "$authors"`
do
#author=`basename $author`
authorID=`expr $authorID + 1`
volumes=`echo "$author"|sed -r 's@[^_]*_PG\s(.*)@\1@g'`
author=`echo "$author"|sed -r 's@([^_]*)_(.*)@\1@g'`
echo "$volumes"
echo 'authorinfo:{'>> "$rootFolder/"$apiFolderAuthors
echo '"authorid"': '"'"$authorID"'"', >> "$rootFolder/"$apiFolderAuthors
echo '"authorname"': '"'`basename "$author"`'"', >> "$rootFolder/"$apiFolderAuthors
echo '"volumes"': '"'"$volumes"'"' >> "$rootFolder/"$apiFolderAuthors

echo '},'>> "$rootFolder/"$apiFolderAuthors

###################

done
echo '}'>> "$rootFolder/"$apiFolderAuthors
	
	
	
}

createtexts() {
IFS='
'
#echo "" > "$rootFolder/"$apiFolderAuthors
#echo "{" > "$rootFolder/"$apiFolderAuthors
 
authors=`find "pmg""/" -mindepth 1 -type d `
authorID=0

echo "$file" >$apiFolderTexts
for author in `echo "$authors"`
do
authorID=`expr $authorID + 1`
#author=`basename $author`
texts=`find "$author" -type f -name '*.txt'`
textID=0

for file in `echo "$texts"`
do




if [ $authorID -gt 222 ]
then
exit
else
#echo "$authorID"-"$textID"
echo "{" >> "$rootFolder/"$apiFolderTexts
textID=`expr $textID + 1`
file=`basename "$file"`
file=`echo "$file" | sed -r 's@(.*)\.pdf\.txt@\1@g'`
echo '"textID"':'"'"$authorID"-"$textID"'"', >> "$rootFolder/"$apiFolderTexts
echo '"filename"':'"'"$file"'"' >> "$rootFolder/"$apiFolderTexts
echo "}," >> "$rootFolder/"$apiFolderTexts



fi
#createtextID $authorID $textID $file 

#cat $file

done

###################

done
echo '}'>> "$rootFolder/"$apiFolderAuthors
	
	
	
}

createFragments() {

IFS='
'
#authors=`find "pmg""/" -mindepth 1`
authors=`find "pmg""/" -mindepth 1 -type d `
authorID=0
for author in `echo "$authors"`
do
#author=`basename $author`
authorID=`expr $authorID + 1`
#echo "$author"
###################
texts=`find "$author" -type f -name '*.txt'`
textID=0
for file in `echo "$texts"`
do
textID=`expr $textID + 1`
if [ $authorID -lt 0 -o $authorID -gt 229 ]
then



#exit
continue
else
#echo "$authorID"-"$textID"
:
fi

createfragmentIDandTokenising $authorID $textID $file 

#cat $file
done
####################
done

}

createwordstree() {
#gives the list of words starting with specific letter
:
	
}






main() {

#apiFolderWords=words
#apiFolderAuthors=authors
	
#createsearchlist
#exit		

	
	
#createFinalJson
#exit

##############################
#authors/id/texts/list
#authors/id/texts/id/content
#authors/texts/id/content
#authors/texts/list
###########################

#/v1/patrologia/authors/info				id_author|list(info) ALL
#/v1/patrologia/authors/id_author/info  		id_author|list(info)



##########################
#ENGINE  to get the contents a LOOP is needed for the urls

#?##/v1/patrologia/authors/words/id_word/urls				(urls with /v1/patrologia/authors/id_author/words/id_word/urls for ALL id_author)

#?##/v1/patrologia/authors/id_author/words/id_word/urls  	(urls with /v1/patrologia/texts/id_text/words/id_word/urls for ALL id_text for the same id_author)

#?##/v1/patrologia/authors/id_author/texts/id_text/words/id_word/urls 		(urls with /v1/patrologia/fragments/id_fragment/content for all id_fragment for the same id_text)

#?##/v1/patrologia/fragments/id_fragment/content            		(content)




##############################

#texts/id_file/content     		 id_file|content     
#texts/id_file/list					id_file|list(info)
#texts/list							id_file|list(info) ALL


#fragments/id_fragments/content    		id_fragments|content 
#fragments/list						id_fragments|list(info) ALL


#words/id_word/content/id_page    		id_word|content 

#words/id_word/list					id_words|list(info)

#words/id_word/content    		id_word|content
 
#words/list						id_words|list(info) ALL



#/devices/{id}/configurations/{id}




cd "$currentDir"
#rm -r "$rootFolder"
#mkdir -p "$rootFolder"


apiFolderFragments=fragments
apiFolderWords=words
apiFolderAuthors=authors
apiFolderTexts=texts

mkdir -p "$rootFolder/$apiFolderWords"
mkdir -p "$rootFolder/$apiFolderFragments"    #/fragments/id_of_fragment
#mkdir -p "$rootFolder/$apiFolderAuthors" /authors #it is a file
#mkdir -p "$rootFolder/$apiFolderTexts"   /texts   #it is a file


#createauthors
#createtexts

#createauthors	
#exit

#createFragments
createFinalJson
#createsearchlist
exit

}
main












#ddddddddddd
