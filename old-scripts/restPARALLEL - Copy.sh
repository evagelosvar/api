#!/bin/bash

#s3cmd put index.html s3://test123123/ --recursive -m text/html --acl-public


	
#cd test

alias filter='sed 's@'\"'@2@g''

alias filter='sed 's@"\'\""@2@g''


#cat 1.txt | xargs -0 -I {} printf "%q\n" {}


cat test/1.txt | tr "<>.\'\"(){}[]+?\-_/\=" '#' | sed -r 's@(–|“|”|‘|#|«|»|©|%|:)@@g'
echo "============"
#cat 1.txt|x
#cat 1.txt>xxx
filtro() {
if [ ! -t 0 ] 
   then
       # INPUT=$(cat) 
INPUT=`cat|\
 tr "<>.\'\"(){}[]+?\-_/\=" '#' | sed -r 's@(–|“|”|‘|#|«|»|©|%|:)@@g'` 
   else
        INPUT=""
fi
    
echo "$INPUT"
	
}

cat test/1.txt |filtro
#exit
#####################
#MAIN ROUTINE
#####################
convertWork() {
workFile="$1"
echo "$workFile"
author_name=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\2@g'|sed -r 's@([^_]*)_(.*)@\1@g'`
work_title=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\3@g'`
work_url="$workFile"

#author_id=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\2@g'|sha1sum|sed -r 's@ \*-@@g'`
author_id=`echo "$author_name"  |sha1sum|sed -r 's@ \*-@@g'`
work_id=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\3@g'|sha1sum|sed -r 's@ \*-@@g'`

id=$author_id-$work_id

#################################
split  --additional-suffix=".frg" -a 5 -d -l 5 "$workFile" fragments/fragment-$id- #fragmnets
#################################

find fragments -name "*$id-*.frg"| while read  fragment
do
######################
#START WORD JSON
#######################

jsonWord=',
{
"fragment_url"	:"'"$fragment"'",
"author_name"	:"'"$author_name"'",
"work_title"	:"'"$work_title"'",
"work_url"		:"'"$work_url"'"
}'
#pass filter
#########################################################
files=`cat $fragment |\
sed -r 's@'"'"'@@g' |\
xargs -n 1 echo words/|sed -r 's@\/ @\/@g' | xargs echo`
#########################################################
echo "$jsonWord" | tee -a `echo "$files"` >/dev/null


######################
#MAKE FRAGMENT JSON
######################
fragment_content=`cat $fragment` #pass filter

jsonFragment='[
{
"fragment_id"		:"'"$fragment"'",
"fragment_content"	:"'"$fragment_content"'",
"author_name"		:"'"$author_name"'",
"work_title"		:"'"$work_title"'",
"work_url"			:"'"$work_url"'"
}
]'

echo "$jsonFragment" > $fragment 
#########################
done

}

#################
#Public Variables
#################
#cd test

corpusFolder=PG_Migne_FULL_test
words=words
fragments=fragments
currentDir=`dirname $0`

#baseFolder=test
cd "$currentDir"
echo "$currentDir"
#cd "$baseFolder"
rm -R "$words"
rm -R "$fragments"
mkdir -p "$words"
mkdir -p "$fragments"
############


parallelExist=`parallel --vversion &>/dev/null;echo $?`

if [ $parallelExist -gt 0 ]
then
alias findCMD='find "$corpusFolder""/" -type f -name "*.txt"|sort'
echo "Not Parallel Execution"
find "$corpusFolder""/" -type f -name "*.txt"|sort|xargs -I % sh convertWork.sh %
find  "words" -type f  -exec sed -i '1{s@,@[@g}' {} +
find  "words" -type f  -exec sed -i '${s@\}@\}\n\]@g}' {} +
exit


else
#alias findCMD='find "$corpusFolder""/" -type f -name "*.txt"|sort| parallel --xargs'
#find "$corpusFolder""/" -type f -name "*.txt"
echo "Parallel Execution"

find "$corpusFolder""/" -type f -name "*.txt"|sort| parallel  sh convertWork.sh {}


find  "words" -type f  -exec sed -i '1{s@,@[@g}' {} +
find  "words" -type f  -exec sed -i '${s@\}@\}\n\]@g}' {} +
exit

fi



#findCMD
#exit
#find "$corpusFolder""/" -type f -name '*.txt'|sort|\






findCMD|\
while read  workFile
do
convertWork "$workFile"

done

find  "words" -type f  -exec sed -i '1{s@,@[@g}' {} +
find  "words" -type f  -exec sed -i '${s@\}@\}\n\]@g}' {} +


exit




