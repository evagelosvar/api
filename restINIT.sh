#!/bin/bash
#############################
#VARIABLES
#############################
#git reset --hard origin/master
#1 ΤΟΥ
corpusFolder=PG_Migne_FULL_test
words=words
fragments=fragments
list=list
api=api
currentDir=`dirname $0`

#baseFolder=test
#cd "$baseFolder"
cd "$currentDir"
echo 'Working directory: '"$currentDir"



rm -R "$words"
rm -R "$fragments"
rm -R "$list"

mkdir -p "$words"
mkdir -p "$fragments"
mkdir -p "$list"




ulimit -n 1000000
#############################
#BEGINNING
#############################
parallelExist=`parallel --version &>/dev/null;echo $?`
if [ "$parallelExist" -gt "0" ]
then
echo "Not Parallel Execution"
find "$corpusFolder""/" -type f -name "*.txt"|sort|xargs -I % sh convertWork.sh %


#find  "words" -type f  -exec sed -i '1{s@,@[@g}' {} +
#find  "words" -type f  -exec sed -i '${s@\}@\}\n\]@g}' {} +

else
echo "Parallel Execution"
find "$corpusFolder""/" -type f -name "*.txt"|sort| parallel  sh convertWork.sh {}

#find  "$words" -type f | parallel  'sed -i  "1{s@,@\[@g}" {}'
#find  "$words" -type f | parallel  'sed -i  "${s@\}@\}\n\]@g}" {}'

fi
echo "FINILISE WORDS"
#MODIFY the JSON fILES to be correct
find  "$words" -type f  -exec sed -i '1{s@,@[@g}' {} +
find  "$words" -type f  -exec sed -i '${s@\}@\}\n\]@g}' {} +
echo "END FINILISE WORDS"
echo "CREATE STEM"
#NEEDS CHECK
#find  "$words" -type f |sort| parallel  sh createSTEM-LIST.sh {}
sh createSTEMS.sh
echo "END CREATE STEM"
echo "FINILISE STEMS"
find  "$list" -type f  -exec sed -i '${s@,@\]@g}' {} +
echo "END FINILISE STEMS"

##############################
#UPLOAD to s3 STORAGE
##############################

 
echo "BEGIN UPLOAD"


cd "$words"
find .| parallel --xargs s3cmd put {/.} s3://testvag/"$words"/ --recursive -m text/html --acl-public
cd ..

cd "$fragments"
find .| parallel --xargs s3cmd put {/.} s3://testvag/"$fragments"/ --recursive -m text/html --acl-public
cd ..

cd "$list"
find .| parallel --xargs s3cmd put {/.} s3://testvag/"$list"/ --recursive -m text/html --acl-public
cd ..




#############################
#INSTAL s3cmd
#############################
#sudo apt-get install python-setuptools
#wget https://sourceforge.net/projects/s3tools/files/s3cmd/2.0.2/s3cmd-2.0.2.tar.gz
#tar xvfz s3cmd-2.0.2.tar.gz
#cd s3cmd-2.0.2
#python setup.py install
#s3cmd --configure


#############################
#INSTAL parallel
#############################
#sudo apt-get install parallel
exit 






#apt-cyg install python-setuptools
#s3cmd put index.html s3://test123123/ --recursive -m text/html --acl-public
#seq 9 -1 1 | parallel --eta -j1 --load 80% --noswap  echo


#cd test


alias filter='sed 's@'\"'@2@g''
alias filter='sed 's@"\'\""@2@g''
#cat 1.txt | xargs -0 -I {} printf "%q\n" {}
cat test/1.txt | tr "<>.\'\"(){}[]+?\-_/\=" '#' | sed -r 's@(–|“|”|‘|#|«|»|©|%|:)@@g'
echo "============"
#cat 1.txt|x
#cat 1.txt>xxx
filtro() {
if [ ! -t 0 ] 
   then
       # INPUT=$(cat) 
	INPUT=`cat|\
tr "<>.\'\"(){}[]+?\-_/\=" '#' | sed -r 's@(–|“|”|‘|#|«|»|©|%|:)@@g'` 
   else
	INPUT=""
fi
   
echo "$INPUT"
}

cat test/1.txt |filtro
