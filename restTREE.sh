#!/bin/bash

#s3cmd put index.html s3://test123123/ --recursive -m text/html --acl-public

cd test
corpusFolder=PG_Migne_FULL
echo '[' >tree
numofAuthors=`find "$corpusFolder""/" -mindepth 1 -type d |sort|wc -l`
count_author=0

find "$corpusFolder""/" -mindepth 1 -type d |sort|\
while read  author_folder
do

count_author=`expr $count_author + 1`
echo $author_folder
author_name=`echo "$author_folder"|sed -r 's@(^.*)/(.*)@\2@g'|sed -r 's@([^_]*)_(.*)@\1@g'`
author_id=`echo "$author_name" |sha1sum|sed -r 's@ \*-@@g'`
volumes=`echo "$author_folder"|sed -r 's@(^.*)/(.*)@\2@g'|sed -r 's@[^_]*_PG\s(.*)@\1@g'`

#echo "$volumes"|cut -d'-' -f1

#exit

jsonTree1='{
"author_id"		:"'"$author_id"'",
"author_name"	:"'"$author_name"'",
"volumes"		:['"$volumes"'],
"works":
['

echo "$jsonTree1" >>tree
count_work=0
numofWorks=`find "$author_folder""/" -type f -name '*.txt'|sort|wc -l`

find "$author_folder""/" -type f -name '*.txt'|sort|\
while read  workFile
do
count_work=`expr $count_work + 1`
echo $workFile
work_title=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\3@g'`
work_id=`echo "$workFile" | sed -r 's@(^.*)/(.*)/(.*)@\3@g'|sha1sum|sed -r 's@ \*-@@g'`

jsonTree2B='{
"work_id"		:"'"$work_id"'",
"work_title"	:"'"$work_title"'"
},'

if [ $count_work -lt $numofWorks ]
then
jsonTree2B='{"work_id"		:"'"$work_id"'","work_title"	:"'"$work_title"'"},'
else
jsonTree2B='{"work_id"		:"'"$work_id"'","work_title"	:"'"$work_title"'"}'
fi

echo "$jsonTree2B" >>tree
done


if [ $count_author -lt $numofAuthors ]
then
jsonTree2E=']
},'

:
else
:
jsonTree2E=']
}
]'
fi

echo "$jsonTree2E" >>tree

done
